/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.rbd.task;

import cn.hutool.core.lang.Validator;
import cn.hutool.json.JSONUtil;
import com.ceph.rados.IoCTX;
import com.ceph.rados.Rados;
import com.ceph.rados.exceptions.ErrorCode;
import com.ceph.rbd.RbdException;
import com.dsms.common.constant.TaskStatusEnum;
import com.dsms.common.constant.TaskTypeEnum;
import com.dsms.common.taskmanager.TaskStrategy;
import com.dsms.common.taskmanager.model.Task;
import com.dsms.common.taskmanager.service.ITaskService;
import com.dsms.common.util.ByteUtil;
import com.dsms.dfsbroker.rbd.model.Rbd;
import com.dsms.dfsbroker.rbd.model.dto.RbdCreateDTO;
import com.dsms.dfsbroker.rbd.service.RbdWrapper;
import com.dsms.dfsbroker.storagepool.model.StoragePool;
import com.dsms.dfsbroker.storagepool.service.IStoragePoolService;
import com.dsms.modules.util.RadosSingleton;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.time.LocalDateTime;

@Slf4j
@Service(TaskTypeEnum.TypeConstants.CREATE_RBD)
public class CreateRbdTask implements TaskStrategy {

    private final ITaskService taskService;
    private final IStoragePoolService storagePoolService;

    @Autowired
    public CreateRbdTask(ITaskService taskService, IStoragePoolService storagePoolService) {
        this.taskService = taskService;
        this.storagePoolService = storagePoolService;
    }

    @Override
    public Task execute(Task task) {
        String taskParam = task.getTaskParam();
        if (ObjectUtils.isEmpty(taskParam)) {
            task.setTaskEndTime(LocalDateTime.now());
            task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
            task.setTaskErrorMessage("task parameter is null");
            return task;
        }
        RbdCreateDTO rbdCreateDTO = JSONUtil.toBean(taskParam, RbdCreateDTO.class);

        if (!Validator.isGeneral(rbdCreateDTO.getRbdName())) {
            throw new IllegalArgumentException("rbdName contains illegal characters");
        }

        StoragePool metaPool = storagePoolService.get(rbdCreateDTO.getPoolName());
        StoragePool dataPool = metaPool;
        if (rbdCreateDTO.getBasedOnEra()) {
            dataPool = storagePoolService.get(rbdCreateDTO.getDataPoolName());
            if (metaPool.getRbdAvailableCapacity() < Rbd.RBD_META_DATA_BYTES) {
                task.setTaskEndTime(LocalDateTime.now());
                task.setTaskErrorMessage("the allocated capacity of the data pool is less than 100M");
                task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
                return task;
            }
            if (ByteUtil.gbToBytes(rbdCreateDTO.getRbdSize()) > dataPool.getRbdAvailableCapacity()) {
                task.setTaskEndTime(LocalDateTime.now());
                task.setTaskErrorMessage("rbd's capacity exceeds the limit");
                task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
                return task;
            }
        } else {
            if (ByteUtil.gbToBytes(rbdCreateDTO.getRbdSize()) > metaPool.getRbdAvailableCapacity()) {
                task.setTaskEndTime(LocalDateTime.now());
                task.setTaskErrorMessage("rbd's capacity exceeds the limit");
                task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
                return task;
            }
        }

        Rados rados = RadosSingleton.INSTANCE.getRados();
        try (IoCTX ioctx = rados.ioCtxCreate(rbdCreateDTO.getPoolName())) {
            if (!rbdCreateDTO.getBasedOnEra()) {
                com.ceph.rbd.Rbd rbd = new com.ceph.rbd.Rbd(ioctx);
                rbd.create(rbdCreateDTO.getRbdName(), ByteUtil.gbToBytes(rbdCreateDTO.getRbdSize()));
            } else {
                RbdWrapper rbdWrapper = new RbdWrapper(ioctx);
                rbdWrapper.create(rbdCreateDTO.getRbdName(), ByteUtil.gbToBytes(rbdCreateDTO.getRbdSize()), rbdCreateDTO.getDataPoolName());
            }
        } catch (RbdException e) {
            task.setTaskErrorMessage(ErrorCode.getErrorMessage(e.getReturnValue()));
            throw new RuntimeException(ErrorCode.getErrorMessage(e.getReturnValue()));
        } catch (IOException e) {
            task.setTaskErrorMessage(e.getMessage());
            throw new RuntimeException(e);
        } finally {
            task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
            task.setTaskEndTime(LocalDateTime.now());
        }
        task.setTaskStatus(TaskStatusEnum.FINISH.getStatus());
        task.setTaskEndTime(LocalDateTime.now());
        return task;
    }

    @Override
    public boolean validateTask(String[] validateParam) {
        return taskService.validateTaskMessageAndTaskType(validateParam[0]);
    }
}
