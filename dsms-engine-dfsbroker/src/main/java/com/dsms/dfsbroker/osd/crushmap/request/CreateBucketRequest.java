/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.osd.crushmap.request;

import com.alibaba.fastjson2.JSON;
import com.dsms.common.constant.CrushFailureDomainEnum;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import org.springframework.http.HttpMethod;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.Map;


public class CreateBucketRequest extends RemoteRequest {

    public static final String CREATE_ROOT_BUCKET_SUCCESS = "added bucket %s type root to crush map";
    public static final String CREATE_HOST_BUCKET_SUCCESS = "added bucket %s type host to location {root=%s}";

    public CreateBucketRequest(String bucketName, int type, String root) {
        if (ObjectUtils.isEmpty(bucketName)) {
            throw new IllegalArgumentException("bucketName must not be blank");
        }

        Map<String, Object> requestParam = new HashMap<>(8);
        requestParam.put("prefix", "osd crush add-bucket");
        requestParam.put("name", bucketName);
        requestParam.put("type", CrushFailureDomainEnum.getCrushFailureDomain(type));
        if (!ObjectUtils.isEmpty(root)) {
            requestParam.put("args", new String[]{"root=" + root});
        }

        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}