/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.cluster.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.dsms.ClusterProperties;
import com.dsms.common.constant.ResultCode;
import com.dsms.common.exception.DsmsEngineException;
import com.dsms.common.model.LoginUser;
import com.dsms.dfsbroker.cluster.api.ClusterApi;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.dfsbroker.cluster.service.IClusterService;
import com.dsms.modules.util.RemoteCallUtil;
import java.util.UUID;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.EnabledIf;
import org.springframework.transaction.annotation.Transactional;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnabledIf(expression = "${dsms.storage.enable}", loadContext = true)
public class ClusterServiceTest {

    @LocalServerPort
    int port;

    @Autowired
    private IClusterService clusterService;

    @Autowired
    private ClusterApi clusterApi;

    @Autowired
    private ClusterProperties properties;

    @MockBean
    private CacheManager cacheManager;
    @MockBean
    private Cache cache;

    @BeforeAll
    public static void initUser() {
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(new LoginUser(1, "admin", "password"), null));
    }

    @BeforeEach
    public void mockDsmsStorageResult() {
        Cluster cluster = properties.getTestCluster();
        RemoteCallUtil.updateRemoteFixedParam(cluster);
        when(cacheManager.getCache("CurrentBindCluster")).thenReturn(cache);
    }

    @Test
    @DisplayName("集群模块crud")
    @Transactional
    void testNormal() {
        // mock a cluster bean
        Cluster cluster = new Cluster();
        cluster.setClusterName("test");
        cluster.setClusterAddress("127.0.0.1");
        cluster.setClusterPort(8003);
        cluster.setAuthKey("xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx");
        cluster.setAdminKey("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx==");
        cluster.setFsid("xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx");
        // assert create feature
        boolean result = clusterService.save(cluster);
        assertTrue(result);
        Cluster clusterEntity = clusterService.getById(cluster.getId());
        assertNotNull(clusterEntity);
        // assert update feature
        clusterEntity.setClusterName("new_name");
        boolean result1 = clusterService.updateById(clusterEntity);
        assertTrue(result1);
        assertEquals(cluster.getId(), clusterEntity.getId());
        Cluster clusterNew = clusterService.getById(clusterEntity.getId());
        assertEquals(clusterNew.getClusterName(), clusterEntity.getClusterName());
        // assert delete feature
        clusterService.removeById(clusterEntity.getId());
        Cluster clusterToDelete = clusterService.getById(clusterEntity.getId());
        assertNull(clusterToDelete);
    }

    @Test
    @DisplayName("集群模块-绑定功能测试")
    void testBind() {
        Cluster testCluster = properties.getTestCluster();
        Cluster bind = clusterService.bind(testCluster);
        Cluster about = clusterService.about();
        assertEquals(bind.getId(), about.getId());
        boolean unbind = clusterService.unbind();
        verify(cache, times(1)).evict(new SimpleKey());
        assertTrue(unbind);
        assertEquals(ResultCode.CLUSTER_NOTBIND.getCode(), assertThrows(DsmsEngineException.class, clusterService::about).getCode());
    }


    @Test
    @DisplayName("集群模块-异常测试")
    void testBindException() {

        Cluster cluster = new Cluster();
        cluster.setClusterAddress("256.256.256.256");
        assertEquals(ResultCode.CLUSTER_ADDRESS_NET_ERROR.getCode(), assertThrows(DsmsEngineException.class, () -> clusterService.bind(cluster)).getCode());
        cluster.setClusterAddress("127.0.0.1");
        cluster.setClusterPort(65535);
        assertEquals(ResultCode.CLUSTER_SERVICEERROR.getCode(), assertThrows(DsmsEngineException.class, () -> clusterService.bind(cluster)).getCode());

        cluster.setClusterPort(port);
        assertEquals(ResultCode.REMOTE_CALL_FAIL.getCode(), assertThrows(DsmsEngineException.class, () -> clusterService.bind(cluster)).getCode());

        Cluster testCluster = properties.getTestCluster();
        testCluster.setAuthKey(UUID.randomUUID().toString());
        assertEquals(ResultCode.CLUSTER_KEY_ERROR.getCode(), assertThrows(DsmsEngineException.class, () -> clusterService.bind(testCluster)).getCode());
    }


}