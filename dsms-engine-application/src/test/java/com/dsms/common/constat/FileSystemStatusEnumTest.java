package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.FileSystemStatusEnum;
import org.junit.jupiter.api.Test;


class FileSystemStatusEnumTest {


    @Test
    void testGetters() {
        FileSystemStatusEnum failed = FileSystemStatusEnum.FAILED;
        assertEquals(0, failed.getStatus());
        assertEquals("failed", failed.getMessage());
    }

}
