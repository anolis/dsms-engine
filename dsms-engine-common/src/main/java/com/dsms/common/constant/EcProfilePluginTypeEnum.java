/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * the plugin type enum used to create ec-profile
 */
@Getter
@AllArgsConstructor
public enum EcProfilePluginTypeEnum {

    JERASURE(0, "jerasure"),
    ISA(1, "isa");

    private final int code;
    private final String type;

    /*
        according to the code return type
        it return "jerasure" default
     */
    public static String getPluginType(int code) {
        EcProfilePluginTypeEnum[] values = EcProfilePluginTypeEnum.values();
        for (EcProfilePluginTypeEnum ecProfilePluginTypeEnum : values) {
            if (ecProfilePluginTypeEnum.getCode() == code) {
                return ecProfilePluginTypeEnum.getType();
            }
        }
        return JERASURE.getType();
    }
}
