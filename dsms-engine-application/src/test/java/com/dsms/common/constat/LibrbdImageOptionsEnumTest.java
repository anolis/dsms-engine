package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.LibrbdImageOptionsEnum;
import org.junit.jupiter.api.Test;

class LibrbdImageOptionsEnumTest {
    @Test
    void testGetByCode() {
        assertEquals(0, LibrbdImageOptionsEnum.RBD_IMAGE_OPTION_FORMAT.getValue());
    }
}
