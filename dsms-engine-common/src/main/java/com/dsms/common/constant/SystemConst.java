/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.constant;

/**
 * System constant
 */
public class SystemConst {

    private SystemConst() {
    }
    /**
     * version file path
     */
    public static final String VERSION_FILE_PATH = "classpath:VERSION";

    /**
     * release version file path
     */
    public static final String RELEASE_VERSION_FILE_PATH = "/etc/dsms/VERSION";

    /**
     * redis storage login token prefix
     */
    public static final String REDIS_TOKEN_PREFIX = "login:";

    /**
     * redis storage temporary login token prefix
     */
    public static final String REDIS_TEMP_TOKEN_PREFIX = "temp:";

    /**
     * https scheme
     */
    public static final String HTTPS_SCHEME = "https";

    /**
     * http scheme
     */
    public static final String HTTP_SCHEME = "http";

    /**
     * dsms-storage validate connect timeout (ms)
     */
    public static final int CONNECT_TIMEOUT = 10000;

    /**
     * dsms-storage default token user
     * TODO: default use admin，Optimize after adding multi-user modules
     */
    public static final String DSMS_STORAGE_USER = "admin";
    /**
     * dsms-storage default auth prefix
     */
    public static final String DSMS_STORAGE_AUTH_PREFIX = "client.";

    /**
     * dsms-storage admin keyring path
     */
    public static final String ADMIN_KEYRING_PATH = "/etc/ceph/ceph.client.admin.keyring";

    /**
     * sms-storage admin keyring name
     */
    public static final String ADMIN_KEYRING_NAME = "ceph.client.admin.keyring";

}
