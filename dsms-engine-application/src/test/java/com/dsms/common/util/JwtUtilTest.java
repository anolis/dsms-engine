package com.dsms.common.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.jsonwebtoken.Claims;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class JwtUtilTest {

    @Test
    public void testJwtUtil() {
        JwtUtil jwtUtil = new JwtUtil();
        jwtUtil.setKey(UUID.randomUUID().toString());
        String userId = "id";
        String userName = "name";
        String newToken = jwtUtil.createJWT(userId, userName, null);
        Claims claims = jwtUtil.parseJWT(newToken);
        assertEquals(userId, claims.getId());
        assertEquals(userName, claims.getSubject());
    }

}