/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagepool.model.dto;

import com.dsms.common.constant.NameSchemeEnum;
import com.dsms.common.validator.GeneralParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/*
    This is the parent class that creates the storage pool object
    It has two subclasses:ErasureCreateDTO and ReplicatedDTO
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class StoragePoolCreateDTO {

    @NotBlank(message = "存储池名称不能为空")
    @GeneralParam(nameSchemeEnum = NameSchemeEnum.STORAGE_POOL)
    private String poolName;
    @NotNull(message = "存储池类型不能为空")
    private int poolType;
    @NotNull(message = "pg数不能为空")
    private Integer pgNum;
    @NotNull(message = "pgp数不能为空")
    private Integer pgpNum;

}
