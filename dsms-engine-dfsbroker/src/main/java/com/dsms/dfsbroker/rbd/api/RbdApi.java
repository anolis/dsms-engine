/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.rbd.api;

import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.common.CommandDirector;
import com.dsms.dfsbroker.common.api.CommonApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RbdApi {
    public static final int MAX_RETRIES = CommonApi.DEFAULT_RETRY_TIMES;

    @Autowired
    private CommonApi commonApi;

    /**
     * get pool which enable performance monitoring for all volumes
     */
    public RemoteResponse getEnabledRbdMonPoolRequest(RemoteRequest remoteRequest) throws Throwable {
        CommandDirector.constructGetEnabledRbdMonPoolRequest(remoteRequest);
        RemoteResponse response = commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
        return commonApi.getRequestResult(remoteRequest, MAX_RETRIES, response.getId());
    }

    /**
     * enable performance monitoring for all volumes in a storage pool
     */
    public RemoteResponse enableRbdMon(RemoteRequest remoteRequest, String poolName) throws Throwable {
        CommandDirector.constructEnableRbdMonRequest(remoteRequest, poolName);
        RemoteResponse response = commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
        return commonApi.getRequestResult(remoteRequest, MAX_RETRIES, response.getId());
    }

}
