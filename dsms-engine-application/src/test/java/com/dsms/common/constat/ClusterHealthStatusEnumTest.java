package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.ClusterHealthStatusEnum;
import org.junit.jupiter.api.Test;


class ClusterHealthStatusEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(0, ClusterHealthStatusEnum.HEALTH_OK.getCode());
        assertEquals("HEALTH_OK", ClusterHealthStatusEnum.HEALTH_OK.getStatus());
    }

}
