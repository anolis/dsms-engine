/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.util;


import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.db.meta.Column;
import cn.hutool.db.meta.MetaUtil;
import cn.hutool.db.meta.Table;
import com.alibaba.druid.DbType;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceWrapper;
import com.dsms.common.constant.DatabaseFieldSubEnum;
import com.dsms.common.model.DatabaseFieldSubBO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class DatabaseFieldUtil implements ApplicationRunner {
    @Autowired
    public DataSource dataSource;

    public static List<DatabaseFieldSubBO> databaseFieldSubList = new ArrayList<>();

    @Override
    public void run(ApplicationArguments args) {
        DruidDataSourceWrapper druidDataSourceWrap = (DruidDataSourceWrapper) dataSource;
        String dbType = druidDataSourceWrap.getDbType();
        for (DatabaseFieldSubEnum value : DatabaseFieldSubEnum.values()) {
            Column column = null;
            if (DbType.mariadb.name().equals(dbType)) {
                String name = value.getClazz().getSimpleName().toLowerCase();
                Table tableMeta = MetaUtil.getTableMeta(dataSource, name);
                if (!ObjectUtils.isEmpty(tableMeta) && !ObjectUtils.isEmpty(tableMeta.getColumns())) {
                    column = tableMeta.getColumn(value.getFieldName().toLowerCase());
                }
            } else if (DbType.h2.name().equals(dbType)) {
                String name = value.getClazz().getSimpleName().toUpperCase();
                Table tableMeta = MetaUtil.getTableMeta(dataSource, name);
                if (!ObjectUtils.isEmpty(tableMeta) && !ObjectUtils.isEmpty(tableMeta.getColumns())) {
                    column = tableMeta.getColumn(value.getFieldName());
                }
            }
            if (column !=null && !ObjectUtils.isEmpty(column)) {
                DatabaseFieldSubBO databaseFieldSubBO = new DatabaseFieldSubBO(value.getClazz(), CharSequenceUtil.toCamelCase(value.getFieldName()), (int) column.getSize());
                databaseFieldSubList.add(databaseFieldSubBO);
            }
        }
    }
}
