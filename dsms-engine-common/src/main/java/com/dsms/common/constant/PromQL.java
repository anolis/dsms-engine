/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.constant;

public class PromQL {

    private PromQL() {
    }

    /**
     * cluster promQL
     */
    public static final String CLUSTER_CPU_USAGE = "avg(1-(avg by(instance)(rate(node_cpu_seconds_total{mode='idle'}[1m]))))";
    public static final String CLUSTER_MEMORY_USAGE = "avg(((node_memory_MemTotal_bytes)-((node_memory_MemFree_bytes)+(node_memory_Cached_bytes)+(node_memory_Buffers_bytes)+(node_memory_Slab_bytes)))/(node_memory_MemTotal_bytes ))";
    public static final String CLUSTER_CAPACITY_USAGE = "sum(ceph_osd_stat_bytes_used)/sum(ceph_osd_stat_bytes)";
    public static final String CLUSTER_WRITE_OPS = "sum(rate(ceph_osd_op_w[1m]))";
    public static final String CLUSTER_READ_OPS = "sum(rate(ceph_osd_op_r[1m]))";
    public static final String CLUSTER_WRITE_BPS = "sum(rate(ceph_osd_op_w_in_bytes[1m]))";
    public static final String CLUSTER_READ_BPS = "sum(rate(ceph_osd_op_r_out_bytes[1m]))";
    public static final String CLUSTER_TOTAL_PG = "sum(ceph_pg_total)";
    public static final String CLUSTER_ACTIVE_PG = "sum(ceph_pg_active)";
    public static final String CLUSTER_INACTIVE_PG = "sum(ceph_pg_total) - sum(ceph_pg_active)";
    public static final String CLUSTER_UNDERSIZED_PG = "sum(ceph_pg_undersized)";
    public static final String CLUSTER_DEGRADED_PG = "sum(ceph_pg_degraded)";
    public static final String CLUSTER_INCONSISTENT_PG = "sum(ceph_pg_inconsistent)";
    public static final String CLUSTER_DOWN_PG = "sum(ceph_pg_down)";
    public static final String CLUSTER_UNKNOWN_PG = "sum(ceph_pg_unknown)";
    public static final String CLUSTER_RECOVERY_OPS = "sum(rate(ceph_osd_recovery_ops[1m]))";
    /**
     * node promQL
     */
    public static final String NODE_CPU_USAGE = "sum by(mode)(rate(node_cpu_seconds_total{instance=~\"%s\",mode=~\"(irq|nice|softirq|steal|system|user|iowait)\"}[1m]))/scalar(sum(rate(node_cpu_seconds_total{instance=~\"%s\"}[1m])))*100";
    public static final String NODE_CPU_USAGE_TOTAL = "sum(sum by(mode)(rate(node_cpu_seconds_total{instance=~\"%s\",mode=~\"(irq|nice|softirq|steal|system|user|iowait)\"}[1m])) /scalar(sum(rate(node_cpu_seconds_total{instance=~\"%s\"}[1m]))))";
    public static final String NODE_MEMORY_TOTAL = "node_memory_MemTotal_bytes{instance=\"%s\"}";
    public static final String NODE_MEMORY_USED = "(node_memory_MemTotal_bytes{instance=\"%s\"})-((node_memory_MemFree_bytes{instance=\"%s\"})+(node_memory_Cached_bytes{instance=\"%s\"})+(node_memory_Buffers_bytes{instance=\"%s\"})+(node_memory_Slab_bytes{instance=\"%s\"}))";
    public static final String NODE_MEMORY_USED_RATE = "(" + NODE_MEMORY_USED + ")" + "/" + "(" + NODE_MEMORY_TOTAL + ")";
    public static final String NODE_NETWORK_RECEIVE_LOAD = "sum by (device) (rate(node_network_receive_bytes_total{instance=\"%s\",device!=\"lo\"}[1m]))";
    public static final String NODE_NETWORK_TRANSMIT_LOAD = "sum by (device) (rate(node_network_transmit_bytes_total{instance=\"%s\",device!=\"lo\"}[1m]))";
    public static final String NODE_NETWORK_RECEIVE_DROP_RATE = "rate(node_network_receive_drop_total{instance=\"%s\"}[1m])";
    public static final String NODE_NETWORK_TRANSMIT_DROP_RATE = "rate(node_network_transmit_drop_total{instance=\"%s\"}[1m])";
    public static final String NODE_NETWORK_RECEIVE_ERROR_RATE = "rate(node_network_receive_errs_total{instance=\"%s\"}[1m])";
    public static final String NODE_NETWORK_TRANSMIT_ERROR_RATE = "rate(node_network_transmit_errs_total{instance=\"%s\"}[1m])";
    public static final String NODE_NETWORK_RETRANS_RATE = "(increase(node_netstat_Tcp_RetransSegs{instance=\"%s\"}[1m])/increase(node_netstat_Tcp_OutSegs{instance=\"%s\"}[1m]))";
    /**
     * storage pool promQL
     */
    public static final String POOL_READ_OPS = "rate(ceph_pool_rd[1m])*on(pool_id) group_left(instance,name) ceph_pool_metadata{name=\"%s\"}";
    public static final String POOL_WRITE_OPS = "rate(ceph_pool_wr[1m])*on(pool_id) group_left(instance,name) ceph_pool_metadata{name=\"%s\"}";
    public static final String POOL_READ_BPS = "rate(ceph_pool_rd_bytes[1m])+on(pool_id) group_left(instance,name) ceph_pool_metadata{name=\"%s\"}";
    public static final String POOL_WRITE_BPS = "rate(ceph_pool_wr_bytes[1m])+on(pool_id) group_left(instance,name) ceph_pool_metadata{name=\"%s\"}";
    /**
     * rbd promQL
     */
    public static final String RBD_READ_OPS = "rate(ceph_rbd_read_ops{pool=\"%s\", image=\"%s\"}[30s])";
    public static final String RBD_WRITE_OPS = "rate(ceph_rbd_write_ops{pool=\"%s\", image=\"%s\"}[30s])";
    public static final String RBD_READ_BPS = "rate(ceph_rbd_read_bytes{pool=\"%s\", image=\"%s\"}[30s])";
    public static final String RBD_WRITE_BPS = "rate(ceph_rbd_write_bytes{pool=\"%s\", image=\"%s\"}[30s])";
    public static final String RBD_READ_LATENCY = "rate(ceph_rbd_read_latency_sum{pool=\"%s\", image=\"%s\"}[30s]) / rate(ceph_rbd_read_latency_count{pool=\"%s\", image=\"%s\"}[30s])";
    public static final String RBD_WRITE_LATENCY = "rate(ceph_rbd_write_latency_sum{pool=\"%s\",image=\"%s\"}[30s]) / rate(ceph_rbd_write_latency_count{pool=\"%s\", image=\"%s\"}[30s])";
    /**
     * file system promQL
     */
    public static final String FS_READ_OPS = "rate(ceph_pool_rd{pool_id=\"%s\"}[1m])";
    public static final String FS_WRITE_OPS = "rate(ceph_pool_wr{pool_id=\"%s\"}[1m])";
    public static final String FS_META_DATA_REQUEST_LOAD = "sum(rate(ceph_mds_server_handle_client_request{ceph_daemon=~\"mds.*\"}[1m]))";
    /**
     * osd promQL
     */
    public static final String OSD_READ_OPS = "rate(ceph_osd_op_r{ceph_daemon=\"%s\"}[1m])";
    public static final String OSD_WRITE_OPS = "rate(ceph_osd_op_w{ceph_daemon=\"%s\"}[1m])";
    public static final String OSD_READ_BPS = "rate(ceph_osd_op_r_out_bytes{ceph_daemon=\"%s\"}[1m])";
    public static final String OSD_WRITE_BPS = "rate(ceph_osd_op_w_in_bytes{ceph_daemon=\"%s\"}[1m])";
    public static final String OSD_APPLY_LATENCY = "ceph_osd_apply_latency_ms{ceph_daemon=\"%s\"}";
    public static final String OSD_COMMIT_LATENCY = "ceph_osd_commit_latency_ms{ceph_daemon=\"%s\"}";
    /**
     * device promQL
     */
    public static final String DEVICE_READ_IOPS = "label_replace(rate(node_disk_reads_completed_total[1m]),\"instance\", \"$1\", \"instance\", \"([^:.]*).*\") and on (instance, device) label_replace(label_replace(ceph_disk_occupation{ceph_daemon=\"%s\"}, \"device\", \"$1\", \"device\", \"/dev/(.*)\"), \"instance\", \"$1\", \"instance\", \"([^:.]*).*\")";
    public static final String DEVICE_WRITE_IOPS = "label_replace(rate(node_disk_writes_completed_total[1m]), \"instance\", \"$1\", \"instance\", \"([^:.]*).*\") and on (instance, device) label_replace(label_replace(ceph_disk_occupation{ceph_daemon=\"%s\"}, \"device\", \"$1\", \"device\", \"/dev/(.*)\"), \"instance\", \"$1\", \"instance\", \"([^:.]*).*\")";
    public static final String DEVICE_READ_BPS = "label_replace(rate(node_disk_read_bytes_total[1m]), \"instance\", \"$1\", \"instance\", \"([^:.]*).*\") and on (instance, device) label_replace(label_replace(ceph_disk_occupation{ceph_daemon=\"%s\"}, \"device\", \"$1\", \"device\", \"/dev/(.*)\"), \"instance\", \"$1\", \"instance\", \"([^:.]*).*\")";
    public static final String DEVICE_WRITE_BPS = "label_replace(rate(node_disk_written_bytes_total[1m]), \"instance\", \"$1\", \"instance\", \"([^:.]*).*\") and on (instance, device) label_replace(label_replace(ceph_disk_occupation{ceph_daemon=\"%s\"}, \"device\", \"$1\", \"device\", \"/dev/(.*)\"), \"instance\", \"$1\", \"instance\", \"([^:.]*).*\")";
    public static final String DEVICE_UTILIZATION = "label_replace(rate(node_disk_io_time_seconds_total[1m]), \"instance\", \"$1\", \"instance\", \"([^:.]*).*\") and on (instance, device) label_replace(label_replace(ceph_disk_occupation{ceph_daemon=\"%s\"}, \"device\", \"$1\", \"device\", \"/dev/(.*)\"), \"instance\", \"$1\", \"instance\", \"([^:.]*).*\")";
}