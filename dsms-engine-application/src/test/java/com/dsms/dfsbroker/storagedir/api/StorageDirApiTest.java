/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagedir.api;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import com.dsms.ClusterProperties;
import com.dsms.common.remotecall.model.RemoteFixedParam;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.dfsbroker.storagedir.model.dto.StorageDirDTO;
import com.dsms.dfsbroker.storagedir.model.remote.SubvolumeLsResponse;
import com.dsms.modules.util.RemoteCallUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.EnabledIf;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@EnabledIf(expression = "${dsms.storage.enable}", loadContext = true)
class StorageDirApiTest {

    private static final String VOL_NAME = "testVol";
    private static final String SUB_NAME = "testSub";
    private static RemoteRequest request;
    @Autowired
    StorageDirApi storageDirApi;
    @Autowired
    private ClusterProperties properties;

    @MockBean
    private CommonApi mockCommonApi;


    @BeforeEach
    public void setUp() {
        Cluster cluster = properties.getTestCluster();
        RemoteCallUtil.updateRemoteFixedParam(cluster);
        request = RemoteCallUtil.generateRemoteRequest();
    }

    @Test
    void getStorageDirList() {
        StorageDirDTO storageDirDTO = new StorageDirDTO();
        storageDirDTO.setVolName(VOL_NAME);
        Assertions.assertThatCode(() -> storageDirApi.getStorageDirList(request, storageDirDTO)).doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void testGetStorageDirListThrowsThrowable() {
        final StorageDirDTO storageDirDTO = new StorageDirDTO(VOL_NAME, SUB_NAME);
        when(mockCommonApi.remoteCallRequest(new RemoteRequest(new RemoteFixedParam()), 3,
            SubvolumeLsResponse[].class)).thenThrow(Throwable.class);
        assertThatThrownBy(() -> storageDirApi.getStorageDirList(request, storageDirDTO))
            .isInstanceOf(Throwable.class);
    }

    @Test
    void getStorageDirInfo() {
        StorageDirDTO storageDirDTO = new StorageDirDTO();
        storageDirDTO.setVolName(VOL_NAME);
        storageDirDTO.setSubName(SUB_NAME);
        Assertions.assertThatCode(() -> storageDirApi.getStorageDirInfo(request, storageDirDTO)).doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void testGetStorageDirInfoThrowsThrowable() {
        final StorageDirDTO storageDirDTO = new StorageDirDTO(VOL_NAME, SUB_NAME);
        when(mockCommonApi.remoteCallRequest(new RemoteRequest(new RemoteFixedParam()), 3,
            SubvolumeLsResponse[].class)).thenThrow(Throwable.class);
        assertThatThrownBy(() -> storageDirApi.getStorageDirInfo(request, storageDirDTO))
            .isInstanceOf(Throwable.class);
    }

    @Test
    void getStorageDirAuthList() {
        StorageDirDTO storageDirDTO = new StorageDirDTO();
        storageDirDTO.setVolName(VOL_NAME);
        storageDirDTO.setSubName(SUB_NAME);
        Assertions.assertThatCode(() -> storageDirApi.getStorageDirAuthList(request, storageDirDTO)).doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void testGetStorageDirAuthListThrowsThrowable() {
        final StorageDirDTO storageDirDTO = new StorageDirDTO(VOL_NAME, SUB_NAME);
        when(mockCommonApi.remoteCallRequest(new RemoteRequest(new RemoteFixedParam()), 3,
            SubvolumeLsResponse[].class)).thenThrow(Throwable.class);
        assertThatThrownBy(() -> storageDirApi.getStorageDirAuthList(request, storageDirDTO))
            .isInstanceOf(Throwable.class);
    }

    @Test
    void createStorageDir() {
        final StorageDirDTO storageDirDTO = new StorageDirDTO(VOL_NAME, SUB_NAME);
        Assertions.assertThatCode(() -> storageDirApi.createStorageDir(request, storageDirDTO)).doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void createStorageDirThrowsThrowable() {
        final StorageDirDTO storageDirDTO = new StorageDirDTO(VOL_NAME, SUB_NAME);
        when(mockCommonApi.remoteCall(request, 3, RemoteResponse.class)).thenThrow(Throwable.class);
        assertThatThrownBy(() -> storageDirApi.createStorageDir(request, storageDirDTO)).isInstanceOf(Throwable.class);
    }

    @Test
    void removeStorageDir() {
        final StorageDirDTO storageDirDTO = new StorageDirDTO(VOL_NAME, SUB_NAME);
        Assertions.assertThatCode(() -> storageDirApi.removeStorageDir(request, storageDirDTO)).doesNotThrowAnyException();
    }

    @Test()
    @SneakyThrows
    void removeStorageDirThrowsThrowable() {
        final StorageDirDTO storageDirDTO = new StorageDirDTO(VOL_NAME, SUB_NAME);
        when(mockCommonApi.remoteCall(request, 3, RemoteResponse.class)).thenThrow(Throwable.class);
        assertThatThrownBy(() -> storageDirApi.removeStorageDir(request, storageDirDTO)).isInstanceOf(Throwable.class);
    }

}
