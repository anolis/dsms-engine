/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.storagedir.task;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dsms.common.constant.RemoteResponseStatusEnum;
import com.dsms.common.constant.StepTypeEnum;
import com.dsms.common.constant.TaskStatusEnum;
import com.dsms.common.constant.TaskTypeEnum;
import com.dsms.common.remotecall.model.FailedDetail;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.common.taskmanager.TaskException;
import com.dsms.common.taskmanager.TaskStrategy;
import com.dsms.common.taskmanager.model.Step;
import com.dsms.common.taskmanager.model.Task;
import com.dsms.common.taskmanager.service.IStepService;
import com.dsms.common.taskmanager.service.ITaskService;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.dfsbroker.storagedir.api.StorageDirApi;
import com.dsms.dfsbroker.storagedir.model.dto.StorageDirDTO;
import com.dsms.modules.util.RemoteCallUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


@Slf4j
@Service(TaskTypeEnum.TypeConstants.CREATE_STORAGE_DIR)
public class CreateStorageDirTask implements TaskStrategy {

    private static final int RETRY_TIME = 3;

    private final StorageDirApi storageDirApi;
    private final CommonApi commonApi;
    private final ITaskService taskService;
    private final IStepService stepService;

    @Autowired
    public CreateStorageDirTask (StorageDirApi storageDirApi, CommonApi commonApi, ITaskService taskService, IStepService stepService) {
        this.storageDirApi = storageDirApi;
        this.commonApi = commonApi;
        this.taskService = taskService;
        this.stepService = stepService;
    }

    @Override
    public Task execute(Task task) {
        String taskParam = task.getTaskParam();
        if (ObjectUtils.isEmpty(taskParam)) {
            task.setTaskEndTime(LocalDateTime.now());
            task.setTaskErrorMessage("task parameter is null");
            task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
            return task;
        }
        StorageDirDTO storageDirDTO = JSONUtil.toBean(taskParam, StorageDirDTO.class);
        try {

            Integer taskId = task.getId();
            List<Step> steps = stepService.list(new LambdaQueryWrapper<Step>().eq(Step::getTaskId, taskId));
            //1.create storage dir step
            Step createStep = steps.stream().filter(tmp -> tmp.getStepType().equals(StepTypeEnum.CREATE_STORAGE_DIR.getType())).findFirst().orElse(new Step());
            executeCreateStorageDirStep(storageDirDTO, createStep);

            //2.auth storage dir step
            Step authStep = steps.stream().filter(tmp -> tmp.getStepType().equals(StepTypeEnum.AUTH_STORAGE_DIR.getType())).findFirst().orElse(new Step());
            executeCreateStorageDirStep(storageDirDTO, authStep);

        } catch (Throwable e) {
            log.error("create storage dir task fail, fail reason:{}", e.getMessage(), e);
            if (ObjectUtils.isEmpty(task.getTaskErrorMessage())) {
                task.setTaskErrorMessage(e.getMessage());
            }
            task.setTaskEndTime(LocalDateTime.now());
            task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
            return task;
        }
        task.setTaskEndTime(LocalDateTime.now());
        task.setTaskStatus(TaskStatusEnum.FINISH.getStatus());

        return task;
    }

    private void executeCreateStorageDirStep(StorageDirDTO storageDirDTO, Step step) {
        step.setStepStatus(TaskStatusEnum.EXECUTING.getStatus());
        try {
            RemoteResponse remoteResponse = null;
            if (StepTypeEnum.CREATE_STORAGE_DIR.getType().equals(step.getStepType())) {
                remoteResponse = storageDirApi.createStorageDir(RemoteCallUtil.generateRemoteRequest(), storageDirDTO);
            } else if (StepTypeEnum.AUTH_STORAGE_DIR.getType().equals(step.getStepType())) {
                remoteResponse = storageDirApi.storageDirAuthorize(RemoteCallUtil.generateRemoteRequest(), storageDirDTO);
            }
            if (ObjectUtils.isEmpty(remoteResponse)) {
                throw new TaskException("execute create storage dir step fail");
            }
            boolean flag = true;
            while (flag) {
                step = getStepResponse(step, remoteResponse);
                if (Objects.equals(step.getStepStatus(), TaskStatusEnum.FINISH.getStatus()) || Objects.equals(step.getStepStatus(), TaskStatusEnum.FAIL.getStatus())) {
                    flag = false;
                }
            }
            if (!Objects.equals(step.getStepStatus(), TaskStatusEnum.FINISH.getStatus())) {
                throw new TaskException(step.getStepErrorMessage());
            }
        } catch (Throwable e) {
            step.setStepStatus(TaskStatusEnum.FAIL.getStatus());
            step.setStepEndTime(LocalDateTime.now());
            if (ObjectUtils.isEmpty(step.getStepErrorMessage())) {
                step.setStepErrorMessage(e.getMessage());
            }
            throw new TaskException(step.getStepErrorMessage());
        } finally {
            step.setStepEndTime(LocalDateTime.now());
            stepService.updateById(step);
        }
    }

    @Override
    public boolean validateTask(String[] validateParam) {
        return taskService.validateTaskMessageAndTaskType(validateParam[0]);
    }

    private Step getStepResponse(Step step, RemoteResponse executeResponse) throws Throwable {
        //get response of step request
        RemoteResponse stepResult = commonApi.getRequestResult(RemoteCallUtil.generateRemoteRequest(), RETRY_TIME, executeResponse.getId());

        //analytic response result
        if (Objects.equals(stepResult.getState(), RemoteResponseStatusEnum.SUCCESS.getMessage())) {
            step.setStepStatus(TaskStatusEnum.FINISH.getStatus());
        } else if (Objects.equals(stepResult.getState(), RemoteResponseStatusEnum.FAILED.getMessage())) {
            List<FailedDetail> failed = stepResult.getFailed();
            String failedMessage = StringUtils.hasText(failed.get(0).getOutb()) ? failed.get(0).getOutb() : failed.get(0).getOuts();
            step.setStepErrorMessage(failedMessage);
            step.setStepStatus(TaskStatusEnum.FAIL.getStatus());
            throw new TaskException(failedMessage);
        } else if (ObjectUtils.isEmpty(stepResult.getState())) {
            throw new TaskException("unknown request state,response:" + JSON.toJSONString(stepResult));
        }
        return step;
    }
}
