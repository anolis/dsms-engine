package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.AlertRuleTypeEnum;
import com.dsms.common.constant.CompareTypeEnum;
import org.junit.jupiter.api.Test;

class CompareTypeEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(CompareTypeEnum.EQUAL, CompareTypeEnum.getCompareType(0));
    }

    @Test
    void testGetWrongCode() {
        assertEquals(CompareTypeEnum.UNKNOWN, CompareTypeEnum.getCompareType(100));
    }

    @Test
    void testGetters() {
        CompareTypeEnum equal = CompareTypeEnum.EQUAL;
        assertEquals(0, equal.getCode());
        assertEquals("等于", equal.getDescription());
    }
}
