package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.RemoteResponseStatusEnum;
import org.junit.jupiter.api.Test;

class RemoteResponseStatusEnumTest {
    @Test
    void testGetters() {
        RemoteResponseStatusEnum success = RemoteResponseStatusEnum.SUCCESS;
        assertEquals(0, success.getStatus());
        assertEquals("success", success.getMessage());
    }
}
