/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert;


import cn.hutool.core.map.MapUtil;
import com.dsms.modules.alert.entity.AlertApprise;
import com.dsms.modules.alert.entity.AlertMessage;
import com.dsms.modules.alert.service.IAlertNotifyHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 *
 */
@Component
@Slf4j
public class AlertNotifyDispatcher {

    private final Map<Integer, IAlertNotifyHandler> alertNotifyHandlerMap;

    public AlertNotifyDispatcher(List<IAlertNotifyHandler> alertNotifyHandlerList) {
        alertNotifyHandlerMap = MapUtil.newHashMap(alertNotifyHandlerList.size());
        alertNotifyHandlerList.forEach(r -> alertNotifyHandlerMap.put(r.getType(), r));
    }


    public void sendNotice(AlertApprise apprise, AlertMessage alertMessage) {
        IAlertNotifyHandler alertNotifyHandler = alertNotifyHandlerMap.get(apprise.getType());
        boolean isSuccess = true;
        try {
            alertNotifyHandler.sendAlertMessage(apprise, alertMessage.getContent());
        } catch (Exception e) {
            isSuccess = false;
            log.error("send alert message error:{}", e.getMessage(), e);
        } finally {
            alertNotifyHandler.updateAlertMessageAppriseStatus(alertMessage, isSuccess);
        }
    }

    public void sendNotice(AlertApprise apprise, String alertMessage) {
        IAlertNotifyHandler alertNotifyHandler = alertNotifyHandlerMap.get(apprise.getType());
        try {
            alertNotifyHandler.sendAlertMessage(apprise, alertMessage);
        } catch (Exception e) {
            log.error("send alert message error:{}", e.getMessage(), e);
        }
    }

}
