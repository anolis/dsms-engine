/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.node.api;


import com.dsms.ClusterProperties;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.dfsbroker.node.model.remote.OrchDeviceLsResult;
import com.dsms.dfsbroker.node.model.remote.ResponseMon;
import com.dsms.modules.util.RemoteCallUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@EnabledIf(expression = "${dsms.storage.enable}", loadContext = true)
class NodeApiTest {
    @Autowired
    private NodeApi nodeApi;

    private static RemoteRequest request;

    @Autowired
    private ClusterProperties properties;

    @BeforeEach
    public void setUp() {
        Cluster cluster = properties.getTestCluster();
        RemoteCallUtil.updateRemoteFixedParam(cluster);
        request = RemoteCallUtil.generateRemoteRequest();
    }

    @Test
    @SneakyThrows
    void getNodeList() {
        List<ResponseMon> nodeList = nodeApi.getNodeList(request);
        assertThat(nodeList, hasItems(hasProperty("name", is(request.getRemoteFixedParam().getHost()))));
    }

    @Test
    @SneakyThrows
    void getNodeInfo() {
        List<OrchDeviceLsResult> orchDeviceLs = nodeApi.getOrchDeviceLs(request, null);
        assertThat(orchDeviceLs, hasItems());
    }

    @Test
    void getOsdDf() {
        Assertions.assertThatCode(() -> nodeApi.getOsdDf(request)).doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void addOsd() {
        RemoteResponse addOsd = nodeApi.addOsd(request, request.getRemoteFixedParam().getHost(), "/dev/sdb");
        assertNotNull(addOsd.getId());
        RemoteResponse addResult = nodeApi.getAddOsdResult(request, addOsd.getId());
        assertEquals(addResult.getId(), addOsd.getId());
    }


    @Test
    @SneakyThrows
    void rmOsd() {
        assertFalse(nodeApi.rmOsdSchedule(request, new String[]{"100"}));
    }


    @Test
    void getRmOsdStatus() {
        Assertions.assertThatCode(() -> nodeApi.getRmOsdScheduleStatus(request)).doesNotThrowAnyException();
    }


}