/*
    dsms-engine 系统初始化ddl
*/

DROP TABLE IF EXISTS `sm_user`;
CREATE TABLE `sm_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(12) NOT NULL DEFAULT '' COMMENT '用户名',
  `status` tinyint(4) NOT NULL COMMENT '状态(0-正常，1-停用)',
  `password` varchar(100) NOT NULL DEFAULT '' COMMENT '密码',
  `last_login_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '最后登录IP',
  `last_login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户表';

INSERT INTO dsms_engine.sm_user(id, username, status, password, last_login_ip, last_login_date) VALUES(1, 'admin', 0, '{noop}R0ck90', '127.0.0.1', NULL);

DROP TABLE IF EXISTS `sm_role`;
CREATE TABLE `sm_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `code` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '角色编码',
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='角色信息表';

INSERT INTO dsms_engine.sm_role(id, code,name) VALUES(1, 'admin','管理员');

DROP TABLE IF EXISTS `sm_user_role`;
CREATE TABLE `sm_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户和角色关联表';

INSERT INTO dsms_engine.sm_user_role(id, user_id,role_id) VALUES(1,1,1);


DROP TABLE IF EXISTS `sm_menu`;
CREATE TABLE `sm_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '菜单名称',
  `path` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '路由地址',
  `parent_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '父菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='菜单权限表';

DROP TABLE IF EXISTS `sm_role_menu`;
CREATE TABLE `sm_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增编号',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='角色和菜单关联表';

DROP TABLE IF EXISTS `cluster`;
CREATE TABLE `cluster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `cluster_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '集群名称',
  `cluster_address` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '集群服务地址',
  `cluster_port` int(11) NOT NULL COMMENT '集群服务端口',
  `bind_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-未绑定，1-已绑定',
  `auth_key` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '集群服务认证key',
  `create_time` datetime NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  `admin_key` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fsid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '集群唯一fsid',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_cluster_address_cluster_port` (`cluster_address`,`cluster_port`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='集群信息表';

DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `task_type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务类型',
  `task_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '任务状态（0-未开始，1-队列中，2-执行中，3-完成，4-失败）',
  `task_message` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务信息',
  `task_param` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '任务参数',
  `task_start_time` datetime NOT NULL DEFAULT current_timestamp() COMMENT '任务开始时间',
  `task_end_time` datetime DEFAULT NULL COMMENT '任务结束时间',
  `task_error_message` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '任务执行错误原因',
  `is_exclusive` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否为独占任务（0-否，1-是）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='任务信息表';

DROP TABLE IF EXISTS `step`;
CREATE TABLE `step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL COMMENT '主任务id',
  `step_name` varchar(32) NOT NULL DEFAULT '' COMMENT '子任务名称',
  `step_type` varchar(32) NOT NULL DEFAULT '' COMMENT '子任务类型',
  `step_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '子任务状态（0-未开始，1-队列中，2-执行中，3-完成，4-失败）',
  `step_message` varchar(128) NOT NULL DEFAULT '' COMMENT '子任务信息',
  `step_param` varchar(255) NOT NULL DEFAULT '' COMMENT '子任务参数',
  `step_start_time` datetime NOT NULL DEFAULT current_timestamp() COMMENT '子任务开始时间',
  `step_end_time` datetime DEFAULT NULL COMMENT '子任务结束时间',
  `step_error_message` varchar(512) DEFAULT NULL COMMENT '子任务执行错误信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='任务步骤表';

DROP TABLE IF EXISTS `alert_rule`;
CREATE TABLE `alert_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '告警规则id',
  `rule_metric` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警规则监控项',
  `rule_promql` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警规则表达式',
  `rule_compare_type` int(11) NOT NULL DEFAULT 0 COMMENT '告警规则比较运算符（0-相等，1-大于，-1-小于，2-不相等）',
  `rule_threshold` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '告警规则阈值',
  `rule_threshold_init` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '告警规则阈值初始值',
  `rule_times` int(11) NOT NULL DEFAULT 0 COMMENT '告警规则周期',
  `rule_times_init` int(11) NOT NULL DEFAULT 0 COMMENT '告警规则周期初始值',
  `rule_module` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '告警规则模块',
  `rule_level` int(11) NOT NULL DEFAULT 0 COMMENT '告警级别（0-提示、1-一般、2-重要、3-紧急）',
  `rule_level_init` int(11) NOT NULL DEFAULT 0 COMMENT '告警级别初始值',
  `last_check_time` datetime NOT NULL DEFAULT current_timestamp() COMMENT '告警规则检测时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='告警规则表';

INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(1, 'cpu.usage', '100 - (avg by (instance) (irate(node_cpu_seconds_total{mode="idle"}[5m])) * 100)', 1, '80', '80', 5, 5, 'nodes', 2, 2);
INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(2, 'memory.usage', '((node_memory_MemTotal_bytes - node_memory_MemFree_bytes - node_memory_Buffers_bytes - node_memory_Cached_bytes) / (node_memory_MemTotal_bytes )) * 100', 1, '80', '80', 5, 5, 'nodes', 2, 2);
INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(3, 'device.usage', '(1-node_filesystem_avail_bytes{mountpoint="/"}/node_filesystem_size_bytes{mountpoint="/"} )*100', 1, '95', '95', 5, 5, 'nodes', 2, 2);
INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(4, 'ceph.health.warn', 'ceph_health_status', 0, '1', '1', 5, 5, 'cluster', 2, 2);
INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(5, 'ceph.health.error', 'ceph_health_status', 0, '2', '2', 5, 5, 'cluster', 2, 3);
INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(6, 'network.drop', '(increase(node_network_receive_drop_total{device!="lo"}[1m]) + increase(node_network_transmit_drop_total{device!="lo"}[1m])) / (increase(node_network_receive_packets_total{device!="lo"}[1m]) + increase(node_network_transmit_packets_total{device!="lo"}[1m]))', 1, '0.0001', '0.0001', 5, 5, 'nodes', 2, 2);
INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(7, 'network.error', '(increase(node_network_receive_errs_total{device!="lo"}[1m]) + increase(node_network_transmit_errs_total{device!="lo"}[1m])) / (increase(node_network_receive_packets_total{device!="lo"}[1m]) + increase(node_network_transmit_packets_total{device!="lo"}[1m]))', 1, '0.0001', '0.0001', 5, 5, 'nodes', 2, 2);
INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(8, 'osd.slow', 'ceph_healthcheck_slow_ops', 1, '0', '0', 5, 5, 'cluster', 2, 2);
INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(9, 'mon.count', 'sum(ceph_mon_quorum_status)', -1, '3', '3', 5, 5, 'cluster', 2, 2);
INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(10, 'osd.down', 'count(ceph_osd_up == 0) / count(ceph_osd_up) * 100', 1, '10', '10', 5, 5, 'cluster', 2, 2);
INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(11, 'pool.capacity', 'ceph_pool_stored / (ceph_pool_stored + ceph_pool_max_avail) * on(pool_id) group_right() ceph_pool_metadata * 100', 1, '90', '90', 5, 5, 'cluster', 2, 2);
INSERT INTO dsms_engine.alert_rule (id, rule_metric, rule_promql, rule_compare_type, rule_threshold, rule_threshold_init, rule_times, rule_times_init, rule_module, rule_level, rule_level_init) VALUES(12, 'pg.inactive', 'ceph_pool_metadata * on(pool_id, instance) group_left() (((ceph_pg_total - ceph_pg_active ) or ceph_pg_total unless ceph_pg_active))', 1, '0', '0', 5, 5, 'storage', 2, 2);


DROP TABLE IF EXISTS `alert_message`;
CREATE TABLE `alert_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '告警信息id',
  `rule_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警规则id',
  `content` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警信息内容',
  `module` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警规则模块',
  `level` int(11) NOT NULL DEFAULT 0 COMMENT '告警级别（0-提示、1-一般、2-重要、3-紧急）',
  `create_time` datetime NOT NULL DEFAULT current_timestamp() COMMENT '告警产生时间',
  `update_time` datetime DEFAULT NULL COMMENT '告警确认时间',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '告警信息状态（0-未读，1-已读）',
  `times` int(11) DEFAULT 1 COMMENT '告警次数',
  `sms_status` int(11) NOT NULL DEFAULT 0 COMMENT '短信通知状态（0-未开启，1-开启但未通知，2-开启且已通知）',
  `email_status` int(11) NOT NULL DEFAULT 0 COMMENT '邮件通知状态（0-未开启，1-开启但未通知，2-开启且已通知）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='告警信息表';

DROP TABLE IF EXISTS `alert_apprise`;
CREATE TABLE `alert_apprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '告警通知id',
  `type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '告警通知类型(0-email,1-sms)',
  `smtp_host` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警通知smtp服务器地址',
  `smtp_port` int(11) NOT NULL DEFAULT 25 COMMENT '告警通知smtp服务器端口',
  `smtp_username` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警通知smtp用户名',
  `smtp_password` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警通知smtp密码',
  `smtp_ssl` tinyint(4) NOT NULL DEFAULT 0 COMMENT '告警通知smtp是否开启ssl(0-关闭,1-开启)',
  `email_from` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警通知发送人邮箱',
  `sms_provider` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警通知短信服务商',
  `sms_provider_host` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警通知短信服务域名',
  `sms_region` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警通知所在地域对应id',
  `sms_access_id` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警短信平台访问密钥id',
  `sms_access_secret` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警短信平台访问密钥key',
  `sms_app_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警应用id',
  `sms_sign_name` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警通知短信签名',
  `sms_template_code` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警通知短信模板code',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '告警通知状态(0-关闭、1-开启)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='告警通知表';


DROP TABLE IF EXISTS `alert_contact`;
CREATE TABLE `alert_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '告警联系人id',
  `contact_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '告警联系人类型(0-email,1-sms)',
  `contact_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警联系人名称',
  `contact_account` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '告警联系人帐号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='告警联系人';