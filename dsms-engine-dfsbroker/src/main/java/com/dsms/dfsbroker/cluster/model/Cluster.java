/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.dfsbroker.cluster.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.dsms.dfsbroker.node.model.Node;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel(value = "Cluster对象", description = "集群信息表")
public class Cluster implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("自增主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("集群名称")
    private String clusterName;

    @ApiModelProperty("集群服务地址")
    private String clusterAddress;

    @ApiModelProperty("集群服务端口")
    private Integer clusterPort;

    @ApiModelProperty("0-未绑定，1-已绑定")
    private Byte bindStatus;

    @ApiModelProperty("Restful API使用的认证key")
    @JsonIgnore
    private String authKey;

    @ApiModelProperty("Rados使用的admin key")
    @JsonIgnore
    private String adminKey;

    @ApiModelProperty("集群fsid")
    private String fsid;

    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    /**
     * 集群节点
     */
    @TableField(exist = false)
    private Node[] nodes;
}
