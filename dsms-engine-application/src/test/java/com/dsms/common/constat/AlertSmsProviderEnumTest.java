package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.dsms.common.constant.AlertSmsProviderEnum;
import org.junit.jupiter.api.Test;

class AlertSmsProviderEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(AlertSmsProviderEnum.ALIYUN, AlertSmsProviderEnum.getByCode("aliyun"));
    }

    @Test
    void testGetWrongCode() {
        assertNull(AlertSmsProviderEnum.getByCode("test"));
    }

    @Test
    void testGetters() {
        AlertSmsProviderEnum aliyun = AlertSmsProviderEnum.ALIYUN;
        assertEquals("aliyun", aliyun.getCode());
        assertEquals("阿里云", aliyun.getName());
    }
}
