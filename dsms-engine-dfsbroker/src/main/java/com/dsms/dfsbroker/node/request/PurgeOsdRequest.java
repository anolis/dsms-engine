/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.node.request;

import com.alibaba.fastjson2.JSON;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpMethod;

public class PurgeOsdRequest extends RemoteRequest {
    public static final String PURGE_SUCCESS_RESULT = "purged osd.%s";
    public static final String OSD_NOT_EXIST_SUCCESS_RESULT = "osd.%s does not exist";
    public static final String PURGE_FAILED_RESULT = "osd.%s is not `down`.";

    public PurgeOsdRequest(Integer osdId) {
        Map<String, Object> requestParam = new HashMap<>(4);
        requestParam.put("prefix", "osd purge-actual");
        requestParam.put("id", osdId);
        requestParam.put("yes_i_really_mean_it", true);
        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}
