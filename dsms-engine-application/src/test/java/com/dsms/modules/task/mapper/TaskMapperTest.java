/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.task.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.dsms.common.taskmanager.model.Task;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskMapperTest {
    @Autowired
    private TaskMapper taskMapper;

    @Test
    @DisplayName("任务模块crud")
    @Transactional
    void testNormal() {
        // mock a task bean
        Task task = new Task();
        task.setTaskName("test");
        task.setTaskMessage("任务描述");
        task.setTaskParam("1231412");
        task.setTaskStatus(1);
        task.setTaskEndTime(LocalDateTime.MIN);
        // assert create feature
        int result = taskMapper.insert(task);
        Assertions.assertNotEquals(result, 0);
        Task taskEntity = taskMapper.selectById(task.getId());
        assertNotNull(taskEntity);
        // assert update feature
        taskEntity.setTaskName("new_name");
        int result1 = taskMapper.updateById(taskEntity);
        Assertions.assertNotEquals(result1, 0);
        assertEquals(task.getId(), taskEntity.getId());
        Task taskNew = taskMapper.selectById(taskEntity.getId());
        assertEquals(taskNew.getTaskName(), taskEntity.getTaskName());
        // assert delete feature
        taskMapper.deleteById(taskEntity.getId());
        Task taskToDelete = taskMapper.selectById(taskEntity.getId());
        assertNull(taskToDelete);
    }

}