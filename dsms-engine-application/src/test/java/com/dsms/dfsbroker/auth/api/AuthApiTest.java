/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.auth.api;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.mockito.Mockito.when;

import com.dsms.ClusterProperties;
import com.dsms.common.remotecall.model.RemoteFixedParam;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.auth.api.dto.AuthApi;
import com.dsms.dfsbroker.auth.model.AuthDTO;
import com.dsms.dfsbroker.auth.model.remote.AuthResponse;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.modules.util.RemoteCallUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.EnabledIf;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@EnabledIf(expression = "${dsms.storage.enable}", loadContext = true)
class AuthApiTest {
    private static RemoteRequest request;
    @Autowired
    private ClusterProperties properties;

    @Autowired
    AuthApi authApi;

    @Mock
    private CommonApi mockCommonApi;

    @BeforeEach
    public void setUp() {
        Cluster cluster = properties.getTestCluster();
        RemoteCallUtil.updateRemoteFixedParam(cluster);
        request = RemoteCallUtil.generateRemoteRequest();
    }

    @Test
    @SneakyThrows
    void testGetAuthKey() {
        AuthDTO authDTO = new AuthDTO("client.admin");
        Assertions.assertThatCode(() -> authApi.getAuthKey(request, authDTO)).doesNotThrowAnyException();
        assertInstanceOf(AuthResponse.class, authApi.getAuthKey(request, authDTO));
    }

    @Test
    @SneakyThrows
    void testGetAuthKeyThrowsThrowable() {
        AuthDTO authDTO = new AuthDTO("client.test");
        when(mockCommonApi.remoteCallRequest(new RemoteRequest(new RemoteFixedParam()), 3,
                AuthResponse.class)).thenThrow(Throwable.class);
        assertThatThrownBy(() -> authApi.getAuthKey(request, authDTO))
                .isInstanceOf(Throwable.class);
    }
}
