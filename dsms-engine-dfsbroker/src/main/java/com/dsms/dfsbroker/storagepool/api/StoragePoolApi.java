/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagepool.api;

import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.common.CommandDirector;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.dfsbroker.storagepool.model.dto.StoragePoolCreateDTO;
import com.dsms.dfsbroker.storagepool.model.remote.DfResponse;
import com.dsms.dfsbroker.storagepool.model.remote.ResponseStoragePool;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class StoragePoolApi {
    public static final int MAX_RETRIES = CommonApi.DEFAULT_RETRY_TIMES;

    @Autowired
    private CommonApi commonApi;

    /**
     * obtain the basic information of storage pools by poolId
     *
     * @return ResponseStoragePool
     */
    public ResponseStoragePool getStoragePoolByPoolId(RemoteRequest remoteRequest, int poolId) throws Throwable {
        CommandDirector.constructGetStoragePoolByPoolIdRequest(remoteRequest, poolId);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, ResponseStoragePool.class);
    }

    /**
     * obtain the basic information list of storage pools
     *
     * @return list of ResponseStoragePool
     */
    public List<ResponseStoragePool> getStoragePoolList(RemoteRequest remoteRequest) throws Throwable {
        CommandDirector.constructStoragePoolLsRequest(remoteRequest);
        ResponseStoragePool[] responseStoragePool = commonApi.remoteCall(remoteRequest, MAX_RETRIES, ResponseStoragePool[].class);
        return Arrays.stream(responseStoragePool).collect(Collectors.toList());
    }

    /**
     * obtain the usage and total capacity of a storage pool
     *
     * @return DfResponse
     */
    public DfResponse getDf(RemoteRequest remoteRequest) throws Throwable {
        CommandDirector.constructGetDfRequest(remoteRequest);
        return commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, DfResponse.class);
    }

    /**
     * create a storage pool
     *
     * @return RemoteResponse
     */
    public RemoteResponse createPool(RemoteRequest remoteRequest, StoragePoolCreateDTO pool) throws Throwable {
        CommandDirector.constructCreatePoolRequest(remoteRequest, pool);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * get the result of create a storage pool
     *
     * @return RemoteResponse
     */
    public RemoteResponse getCreatePoolResult(RemoteRequest remoteRequest, String requestId) throws Throwable {
        CommandDirector.constructGetRequest(remoteRequest, requestId);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * delete a storage pool
     *
     * @return RemoteResponse
     */
    public RemoteResponse deletePool(RemoteRequest remoteRequest, String poolName) throws Throwable {
        CommandDirector.constructDeletePoolRequest(remoteRequest, poolName);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * allow erasure coded pool overwrites
     *
     * @return RemoteResponse
     */
    public RemoteResponse allowEcOverWrites(RemoteRequest remoteRequest, String poolName) throws Throwable {
        CommandDirector.constructAllowEcOverWritesRequest(remoteRequest, poolName);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

}
