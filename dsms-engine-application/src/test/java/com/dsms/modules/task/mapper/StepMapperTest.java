/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.task.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.dsms.common.taskmanager.model.Step;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StepMapperTest {
    @Autowired
    private StepMapper stepMapper;


    @Test
    @DisplayName("子任务模块crud")
    void testNormal() {
        // mock a step bean
        Step step = new Step(null, 1, "test", "create", 1, "子任务描述", "1111", LocalDateTime.MIN, LocalDateTime.MIN, null, null);
        // assert create feature
        int result = stepMapper.insert(step);
        Assertions.assertNotEquals(0, result);
        Step stepEntity = stepMapper.selectById(step.getId());
        assertNotNull(stepEntity);
        // assert update feature
        stepEntity.setStepName("new_name");
        int updateResult = stepMapper.updateById(stepEntity);
        Assertions.assertNotEquals(0, updateResult);
        assertEquals(step.getId(), stepEntity.getId());
        Step stepNew = stepMapper.selectById(stepEntity.getId());
        assertEquals(stepNew.getStepName(), stepEntity.getStepName());
        // assert delete feature
        stepMapper.deleteById(stepEntity.getId());
        Step stepToDelete = stepMapper.selectById(stepEntity.getId());
        assertNull(stepToDelete);
    }

}