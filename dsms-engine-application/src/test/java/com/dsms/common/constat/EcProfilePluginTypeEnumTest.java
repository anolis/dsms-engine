package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.EcProfilePluginTypeEnum;
import org.junit.jupiter.api.Test;

class EcProfilePluginTypeEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(EcProfilePluginTypeEnum.JERASURE.getType(), EcProfilePluginTypeEnum.getPluginType(0));
        assertEquals(EcProfilePluginTypeEnum.ISA.getType(), EcProfilePluginTypeEnum.getPluginType(1));
    }

    @Test
    void testGetWrongCode() {
        assertEquals(EcProfilePluginTypeEnum.JERASURE.getType(), EcProfilePluginTypeEnum.getPluginType(-1));
    }
}
