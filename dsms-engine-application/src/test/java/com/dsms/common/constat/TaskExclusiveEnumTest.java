package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.TaskExclusiveEnum;
import org.junit.jupiter.api.Test;


class TaskExclusiveEnumTest {


    @Test
    void testGetters() {
        TaskExclusiveEnum nonExclusive = TaskExclusiveEnum.NON_EXCLUSIVE;
        assertEquals(0, nonExclusive.getCode());
        assertEquals("non_exclusive", nonExclusive.getType());
    }

}
