/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.service.impl;

import static com.dsms.modules.alert.service.impl.AlertAppriseServiceImpl.ALERT_TEST_CONTENT;
import static com.dsms.modules.alert.service.impl.AlertAppriseServiceImpl.ALERT_TEST_TITLE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;

import com.dsms.common.constant.AlertAppriseTypeEnum;
import com.dsms.common.constant.AlertSmsProviderEnum;
import com.dsms.common.constant.CommonEnum;
import com.dsms.common.constant.ResultCode;
import com.dsms.common.exception.DsmsEngineException;
import com.dsms.modules.alert.entity.AlertApprise;
import com.dsms.modules.alert.entity.AlertContact;
import com.dsms.modules.alert.service.IAlertContactService;
import com.dsms.modules.mail.model.MailMessage;
import com.dsms.modules.mail.service.IMailSendService;
import com.dsms.modules.sms.model.SmsSendMessage;
import com.dsms.modules.sms.service.ISmsSendService;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AlertAppriseServiceImplTest {

    private final static String smtpUsername = "smtpUsername";
    @Autowired
    private IAlertContactService mockAlertContactService;
    @MockBean
    private IMailSendService mockMailService;
    @MockBean
    private ISmsSendService mockSmsSendService;
    @Autowired
    private AlertAppriseServiceImpl alertAppriseServiceImplUnderTest;

    @Test
    void testSaveOrUpdateAlertApprise() {
        // Setup
        AlertApprise alertApprise = initAlert(AlertAppriseTypeEnum.EMAIL.getCode());

        // Run the test
        final boolean result = alertAppriseServiceImplUnderTest.saveOrUpdateAlertApprise(alertApprise);

        // Verify the results
        assertThat(result).isTrue();
    }

    @Test
    void testSendTestEmail() {
        final boolean result = alertAppriseServiceImplUnderTest.saveOrUpdateAlertApprise(initAlert(AlertAppriseTypeEnum.EMAIL.getCode()));
        assertThat(result).isTrue();

        // Run the test
        final boolean sendTestEmail = alertAppriseServiceImplUnderTest.sendTestEmail();

        // Verify the results
        assertThat(sendTestEmail).isTrue();
        verify(mockMailService).sendMail(new MailMessage(smtpUsername, ALERT_TEST_TITLE, ALERT_TEST_CONTENT, Collections.emptyList()));
    }

    @Test
    void testSendTestEmailIAlertContactServiceReturnsNoItems() {
        // Run the test, has no email item
        assertEquals(ResultCode.ALERT_APPRISE_PARAM_ERROR.getCode(), assertThrows(DsmsEngineException.class, alertAppriseServiceImplUnderTest::sendTestEmail).getCode());
    }

    @Test
    void testSendTestSms() {
        AlertApprise alertApprise = initAlert(AlertAppriseTypeEnum.SMS.getCode());
        final boolean result = alertAppriseServiceImplUnderTest.saveOrUpdateAlertApprise(initAlert(AlertAppriseTypeEnum.SMS.getCode()));
        assertThat(result).isTrue();

        // Run the test
        final boolean sendTestSms = alertAppriseServiceImplUnderTest.sendTestSms();
        assertThat(sendTestSms).isTrue();

        // Verify the results
        SmsSendMessage smsSendMessage = new SmsSendMessage();
        smsSendMessage.setSmsProvider(alertApprise.getSmsProvider());
        smsSendMessage.setSmsSignName(alertApprise.getSmsSignName());
        smsSendMessage.setSmsTemplate(alertApprise.getSmsTemplateCode());
        smsSendMessage.setMobiles(new String[0]);
        verify(mockSmsSendService).doSendSms(smsSendMessage);
    }

    @Test
    void testSendTestSmsIAlertContactServiceReturnsNoItems() {
        assertEquals(ResultCode.ALERT_APPRISE_PARAM_ERROR.getCode(), assertThrows(DsmsEngineException.class, alertAppriseServiceImplUnderTest::sendTestSms).getCode());
    }

    @Test
    void testListNotifyAlertApprise() {
        // Setup
        AlertApprise alertApprise = initAlert(AlertAppriseTypeEnum.EMAIL.getCode());
        final boolean result = alertAppriseServiceImplUnderTest.saveOrUpdateAlertApprise(alertApprise);
        assertThat(result).isTrue();

        // Run the test
        final List<AlertApprise> listResult = alertAppriseServiceImplUnderTest.listNotifyAlertApprise();

        // Verify the results
        assertThat(listResult.get(0).getType()).isEqualTo(alertApprise.getType());
    }

    @Test
    void testListNotifyAlertAppriseIAlertContactServiceReturnsNoItems() {
        // Run the test
        final List<AlertApprise> result = alertAppriseServiceImplUnderTest.listNotifyAlertApprise();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testValidateAlertApprise() {
        // Setup
        AlertApprise alertApprise = initAlert(0);
        alertApprise.setSmtpHost("");
        assertEquals(ResultCode.EMAIL_HOST_EMPTY.getCode(), assertThrows(DsmsEngineException.class, () -> alertAppriseServiceImplUnderTest.validateAlertApprise(alertApprise)).getCode());
    }

    AlertApprise initAlert(int type) {
        // Setup
        final AlertApprise alertApprise = new AlertApprise();
        alertApprise.setId(0);
        alertApprise.setType(type);
        alertApprise.setSmtpHost("smtpHost");
        alertApprise.setSmtpPort(0);
        alertApprise.setSmtpUsername(smtpUsername);
        alertApprise.setSmtpPassword("smtpPassword");
        alertApprise.setEmailFrom("emailFrom");
        alertApprise.setSmsProvider(AlertSmsProviderEnum.ALIYUN.getCode());
        alertApprise.setSmsAccessId("smsAccessId");
        alertApprise.setSmsAccessSecret("smsAccessSecret");
        alertApprise.setSmsSignName("smsSignName");
        alertApprise.setSmsTemplateCode("smsTemplate");
        alertApprise.setSmsProviderHost("smsProviderHost");
        alertApprise.setStatus(CommonEnum.ENABLE.getCode());
        final AlertContact alertContact = new AlertContact();
        alertContact.setContactType(0);
        alertContact.setContactAccount("contactAccount");
        alertApprise.setContacts(List.of(alertContact));
        return alertApprise;
    }
}
