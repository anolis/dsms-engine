/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.remotecall.service.impl;

import com.dsms.ClusterProperties;
import com.dsms.common.remotecall.model.RemoteFixedParam;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.modules.util.RemoteCallUtil;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit.jupiter.EnabledIf;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnabledIf(expression = "${dsms.storage.enable}", loadContext = true)
class RemoteCallImplTest {

    @Autowired
    private ClusterProperties properties;
    @Autowired
    RemoteCallImpl remoteCall;

    private static RemoteRequest request;

    @BeforeEach
    public void setUp() {
        Cluster cluster = properties.getTestCluster();
        RemoteCallUtil.updateRemoteFixedParam(cluster);
        request = RemoteCallUtil.generateRemoteRequest();
    }

    @Test
    void callRestfulApi() {
        request.setUrlPrefix("doc");
        request.setHttpMethod(HttpMethod.GET);
        Assertions.assertThatCode(() -> remoteCall.callRestfulApi(request)).doesNotThrowAnyException();
    }

    @Test
    void callRestfulApiConnectRefused() {
        request.setUrlPrefix("doc");
        request.setHttpMethod(HttpMethod.GET);
        RemoteFixedParam remoteFixedParam = request.getRemoteFixedParam();
        remoteFixedParam.setPort(remoteFixedParam.getPort() + 1);
        request.setRemoteFixedParam(remoteFixedParam);
        Assertions.assertThatCode(() -> remoteCall.callRestfulApi(request)).hasNoSuppressedExceptions();
    }
}