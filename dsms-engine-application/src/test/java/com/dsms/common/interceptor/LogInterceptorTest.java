package com.dsms.common.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class LogInterceptorTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LogInterceptor logInterceptor;

    private HttpServletRequest request;
    private HttpServletResponse response;

    @BeforeEach
    public void setUp() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
    }

    @Test
    void testPreHandleShouldAddTraceIdToMDC() {
        // 执行preHandle方法
        boolean result = logInterceptor.preHandle(request, response, null);

        // 验证结果为true
        assertTrue(result);

        // 获取当前线程的traceId
        String traceIdFromMDC = MDC.get("traceId");

        // 检查traceId是否正确生成并添加到MDC
        assertNotNull(traceIdFromMDC);
        assertEquals(36, traceIdFromMDC.length()); // UUID字符串长度为36

        // 验证MDC的值与生成的UUID一致
        String generatedTraceId = UUID.randomUUID().toString();
        assertTrue(generatedTraceId.matches("\\b[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[0-9a-f]{4}-[0-9a-f]{12}\\b"));
    }

    @Test
    void testAfterCompletionShouldRemoveTraceIdFromMDC() {
        // 先调用preHandle以添加traceId到MDC
        logInterceptor.preHandle(request, response, null);

        String traceIdBefore = MDC.get("traceId");
        assertNotNull(traceIdBefore);

        // 调用afterCompletion方法，移除traceId
        logInterceptor.afterCompletion(request, response, null, null);

        // 检查traceId是否已被移除
        String traceIdAfter = MDC.get("traceId");
        assertNull(traceIdAfter);
    }

    @Test
    void testPreHandleWhenMultipleRequestsShouldGenerateUniqueTraceIds() {
        // 调用preHandle三次，每次生成不同的traceId
        logInterceptor.preHandle(request, response, null);
        String traceId1 = MDC.get("traceId");

        logInterceptor.preHandle(request, response, null);
        String traceId2 = MDC.get("traceId");

        logInterceptor.preHandle(request, response, null);
        String traceId3 = MDC.get("traceId");

        // 验证每次生成的traceId都是不同的
        assertNotEquals(traceId1, traceId2);
        assertNotEquals(traceId2, traceId3);
        assertNotEquals(traceId1, traceId3);
    }
}
