/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagepool.model.remote;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * dsms-storage (df -f json) execute result
 */
@NoArgsConstructor
@Data
public class DfResponse {
    @JsonProperty("stats")
    private StatsDTO stats;
    @JsonProperty("stats_by_class")
    private StatsByClassDTO statsByClass;
    @JsonProperty("pools")
    private List<PoolsDTO> pools;

    //cluster global usage statistics Data Transfer Object
    @NoArgsConstructor
    @Data
    public static class StatsDTO {
        @JsonProperty("total_bytes")
        private Long totalBytes;
        @JsonProperty("total_avail_bytes")
        private Long totalAvailBytes;
        @JsonProperty("total_used_bytes")
        private Long totalUsedBytes;
        //shows a sum of 'USED' space and space allocated/reserved at block device for Ceph purposes
        @JsonProperty("total_used_raw_bytes")
        private Long totalUsedRawBytes;
        @JsonProperty("total_used_raw_ratio")
        private Double totalUsedRawRatio;
        @JsonProperty("num_osds")
        private Integer numOsds;
        @JsonProperty("num_per_pool_osds")
        private Integer numPerPoolOsds;
        //the number of osd used as object storage per pool
        @JsonProperty("num_per_pool_omap_osds")
        private Integer numPerPoolOmapOsds;
    }

    //cluster usage statistics Data Transfer Object  by device class
    @NoArgsConstructor
    @Data
    public static class StatsByClassDTO {
        @JsonProperty("hdd")
        private HddDTO hdd;

        @NoArgsConstructor
        @Data
        public static class HddDTO {
            @JsonProperty("total_bytes")
            private Long totalBytes;
            @JsonProperty("total_avail_bytes")
            private Long totalAvailBytes;
            @JsonProperty("total_used_bytes")
            private Long totalUsedBytes;
            //shows a sum of 'USED' space and space allocated/reserved at block device for Ceph purposes
            @JsonProperty("total_used_raw_bytes")
            private Long totalUsedRawBytes;
            @JsonProperty("total_used_raw_ratio")
            private Double totalUsedRawRatio;
        }
    }

    //pool usage statistics Data Transfer Object
    @NoArgsConstructor
    @Data
    public static class PoolsDTO {
        @JsonProperty("name")
        private String name;
        @JsonProperty("id")
        private Integer id;
        @JsonProperty("stats")
        private StatsDTO stats;

        //pool's usage statistics Data Transfer Object
        @NoArgsConstructor
        @Data
        public static class StatsDTO {

            //bytes stored in the pool not including copies
            @JsonProperty("stored")
            private Long stored;
            //the number of storaged objects
            @JsonProperty("objects")
            private Integer objects;
            @JsonProperty("kb_used")
            private Long kbUsed;
            //warning:this field used to be the bytes used in pool including copies,but it is not stable,now use "storedRaw" instead
            @JsonProperty("bytes_used")
            private Long bytesUsed;
            @JsonProperty("percent_used")
            private Double percentUsed;
            //the maximum available space of the storage pool, calculated by the crush algorithm
            @JsonProperty("max_avail")
            private Long maxAvail;
            //raw avail space,it is calculated from the available capacity of osd
            @JsonProperty("avail_raw")
            private Long availRaw;
            //bytes used in pool including copies made
            @JsonProperty("stored_raw")
            private Long storedRaw;
        }
    }
}
