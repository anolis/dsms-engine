/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.constant;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AlertRuleModuleEnum {


    CLUSTER("cluster", "集群", "集群 ${metric} 指标 ${compare} ${threshold}, 请及时处理"),
    STORAGE("storage", "存储", "${name} ${metric} 指标 ${compare} ${threshold}, 请及时处理"),
    NODE("nodes", "节点", "${instance} 节点 ${metric} 指标 ${compare} ${threshold}, 请及时处理"),
    DEVICES("devices", "设备", "${instance} 节点的 ${device} ${metric} 指标 ${compare} ${threshold}, 请及时处理"),
    DEFAULT("default", "默认", "${metric} 指标 ${compare} ${threshold}, 请及时处理");

    private final String moduleCode;
    private final String moduleName;
    private final String moduleAlertTemplate;

    public static AlertRuleModuleEnum getAlertRuleModuleEnum(String moduleCode) {
        for (AlertRuleModuleEnum alertRuleModuleEnum : AlertRuleModuleEnum.values()) {
            if (alertRuleModuleEnum.getModuleCode().equals(moduleCode)) {
                return alertRuleModuleEnum;
            }
        }
        return DEFAULT;
    }

    public static final String INSTANCE = "instance";
    public static final String METRIC = "metric";
    public static final String COMPARE = "compare";
    public static final String THRESHOLD = "threshold";
    public static final String DEVICE = "device";
    public static final String RULE_NAME = "name";

}
