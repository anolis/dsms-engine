/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.system.service;

import com.dsms.modules.system.model.SmUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SmUserServiceTest {

    @Autowired
    private ISmUserService userService;

    @Test
    @Transactional
    public void testNormal() throws Exception {
        // mock a user bean
        SmUser user = new SmUser();
        user.setUsername("test");
        // assert create feature
        boolean result = userService.save(user);
        assertTrue(result);
        SmUser userEntity = userService.getById(user.getId());
        assertNotNull(userEntity);
        // assert update feature
        userEntity.setUsername("new_name");
        boolean result1 = userService.updateById(userEntity);
        assertTrue(result1);
        assertEquals(user.getId(), userEntity.getId());
        SmUser userNew = userService.getById(userEntity.getId());
        assertEquals(userNew.getUsername(), userEntity.getUsername());
        // assert delete feature
        userService.removeById(userEntity.getId());
        SmUser userToDelete = userService.getById(userEntity.getId());
        assertNull(userToDelete);
    }


}