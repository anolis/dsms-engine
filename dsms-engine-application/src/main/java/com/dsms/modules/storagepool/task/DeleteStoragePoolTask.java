/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.storagepool.task;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dsms.common.constant.*;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.common.taskmanager.TaskStrategy;
import com.dsms.common.taskmanager.model.Step;
import com.dsms.common.taskmanager.model.Task;
import com.dsms.common.taskmanager.service.IStepService;
import com.dsms.common.taskmanager.service.ITaskService;
import com.dsms.dfsbroker.node.model.Device;
import com.dsms.dfsbroker.node.model.dto.NodeDto;
import com.dsms.dfsbroker.osd.crushmap.api.CrushmapApi;
import com.dsms.dfsbroker.osd.crushmap.model.remote.CrushmapBucket;
import com.dsms.dfsbroker.osd.crushmap.request.DelBucketRequest;
import com.dsms.dfsbroker.osd.ecprofile.api.EcProfileApi;
import com.dsms.dfsbroker.osd.osd.api.OsdApi;
import com.dsms.dfsbroker.storagepool.api.StoragePoolApi;
import com.dsms.dfsbroker.storagepool.model.dto.StoragePoolDeleteDTO;
import com.dsms.dfsbroker.storagepool.request.DeleteStoragePoolRequest;
import com.dsms.dfsbroker.storagepool.service.IStoragePoolService;
import com.dsms.modules.util.RemoteCallUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


@Slf4j
@Service(TaskTypeEnum.TypeConstants.DELETE_POOL)
public class DeleteStoragePoolTask implements TaskStrategy {

    private final OsdApi osdApi;
    private final CrushmapApi crushmapApi;
    private final StoragePoolApi storagePoolApi;
    private final ITaskService taskService;
    private final IStepService stepService;
    private final IStoragePoolService storagePoolService;
    private final EcProfileApi ecProfileApi;
    private final GetCrushmapStepResult getCrushmapStepResult;

    @Autowired
    public DeleteStoragePoolTask(OsdApi osdApi, CrushmapApi crushmapApi, StoragePoolApi storagePoolApi, ITaskService taskService,
        IStepService stepService, IStoragePoolService storagePoolService, EcProfileApi ecProfileApi, GetCrushmapStepResult getCrushmapStepResult) {
        this.osdApi = osdApi;
        this.crushmapApi = crushmapApi;
        this.storagePoolApi = storagePoolApi;
        this.taskService = taskService;
        this.stepService = stepService;
        this.storagePoolService = storagePoolService;
        this.ecProfileApi = ecProfileApi;
        this.getCrushmapStepResult = getCrushmapStepResult;
    }

    @Override
    public Task execute(Task task) {
        String taskParam = task.getTaskParam();
        if (ObjectUtils.isEmpty(taskParam)) {
            task.setTaskEndTime(LocalDateTime.now());
            task.setTaskErrorMessage("task parameter is null");
            task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
            return task;
        }
        StoragePoolDeleteDTO storagePoolDeleteDTO = JSONUtil.toBean(taskParam, StoragePoolDeleteDTO.class);
        String poolName = storagePoolDeleteDTO.getPoolName();
        Integer poolType = storagePoolDeleteDTO.getPoolType();
        //1.get step task
        Integer taskId = task.getId();
        List<Step> steps = stepService.list(new LambdaQueryWrapper<Step>().eq(Step::getTaskId, taskId));

        //2.move osd to them default host
        Step moveOsdToDefaultNodeStep = moveOsdToDefaultNode(poolName,
                steps.stream().filter(s -> s.getStepType().equals(StepTypeEnum.RETURN_OSD_TO_NODE.getType())).findFirst().orElse(new Step()));
        if (!Objects.equals(moveOsdToDefaultNodeStep.getStepStatus(), TaskStatusEnum.FINISH.getStatus())) {
            task.setTaskErrorMessage(moveOsdToDefaultNodeStep.getStepErrorMessage());
            task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
            return task;
        }

        //3.delete pool
        Step deletePoolStep = deletePool(poolName,
                steps.stream().filter(s -> s.getStepType().equals(StepTypeEnum.DELETE_POOL.getType())).findFirst().orElse(new Step()));
        if (!Objects.equals(deletePoolStep.getStepStatus(), TaskStatusEnum.FINISH.getStatus())) {
            task.setTaskErrorMessage(deletePoolStep.getStepErrorMessage());
            task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
            return task;
        }

        //4.delete rule
        Step deleteRuleStep = deleteRule(poolName,
                steps.stream().filter(s -> s.getStepType().equals(StepTypeEnum.DELETE_RULE.getType())).findFirst().orElse(new Step()));
        if (!Objects.equals(deleteRuleStep.getStepStatus(), TaskStatusEnum.FINISH.getStatus())) {
            task.setTaskErrorMessage(deleteRuleStep.getStepErrorMessage());
            task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
            return task;
        }

        //5.delete bucket
        Step deletePoolBucketStep = deletePoolBucket(poolName,
                steps.stream().filter(s -> s.getStepType().equals(StepTypeEnum.DELETE_POOL_BUCKET.getType())).findFirst().orElse(new Step()));
        if (!Objects.equals(deletePoolBucketStep.getStepStatus(), TaskStatusEnum.FINISH.getStatus())) {
            task.setTaskErrorMessage(deletePoolBucketStep.getStepErrorMessage());
            task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
            return task;
        }

        //6.delete ec-profile if it is erasure storage pool
        if (Objects.equals(poolType, StoragePoolTypeEnum.ERASURE.getCode())) {
            Step deleteEcProfileStep = deleteEcProfile(poolName,
                    steps.stream().filter(s -> s.getStepType().equals(StepTypeEnum.DELETE_ECPROFILE.getType())).findFirst().orElse(new Step()));
            if (!Objects.equals(deleteEcProfileStep.getStepStatus(), TaskStatusEnum.FINISH.getStatus())) {
                task.setTaskErrorMessage(deleteEcProfileStep.getStepErrorMessage());
                task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
                return task;
            }
        }

        task.setTaskEndTime(LocalDateTime.now());
        task.setTaskStatus(TaskStatusEnum.FINISH.getStatus());
        return task;
    }

    private Step deletePool(String poolName, Step step) {
        //update step status to executing
        step.setStepStatus(TaskStatusEnum.EXECUTING.getStatus());
        try {
            RemoteResponse createResponse = storagePoolApi.deletePool(RemoteCallUtil.generateRemoteRequest(), poolName);
            boolean flag = true;//use to check whether the task in cluster is complete
            while (flag) {
                step = getCrushmapStepResult.getStepResponse(step, createResponse, String.format(DeleteStoragePoolRequest.DELETE_POOL_SUCCESS, poolName), ResultCode.POOL_DELETE_ERROR.getMessage());
                if (Objects.equals(step.getStepStatus(), TaskStatusEnum.FINISH.getStatus()) || Objects.equals(step.getStepStatus(), TaskStatusEnum.FAIL.getStatus())) {
                    flag = false;
                }
            }
        } catch (Throwable e) {
            step.setStepStatus(TaskStatusEnum.FAIL.getStatus());
        }
        return step;
    }

    private Step deleteRule(String poolName, Step step) {
        //update step status to executing
        step.setStepStatus(TaskStatusEnum.EXECUTING.getStatus());
        try {
            RemoteResponse deleteCrushRuleResponse = crushmapApi.delCrushRule(RemoteCallUtil.generateRemoteRequest(), poolName);
            boolean flag = true;//use to check whether the task in cluster is complete
            while (flag) {
                step = getCrushmapStepResult.getStepResponse(step, deleteCrushRuleResponse, "", ResultCode.POOL_DELETE_ERROR.getMessage());
                if (Objects.equals(step.getStepStatus(), TaskStatusEnum.FINISH.getStatus()) || Objects.equals(step.getStepStatus(), TaskStatusEnum.FAIL.getStatus())) {
                    flag = false;
                }
            }
        } catch (Throwable e) {
            step.setStepStatus(TaskStatusEnum.FAIL.getStatus());
        }
        return step;
    }

    private Step moveOsdToDefaultNode(String poolName, Step step) {
        //update step status to executing
        step.setStepStatus(TaskStatusEnum.EXECUTING.getStatus());
        //get pool's all used node and osd
        List<NodeDto> nodeDtoList = storagePoolService.listUsedDisk(poolName);
        if (nodeDtoList.isEmpty()) {
            step.setStepStatus(TaskStatusEnum.FINISH.getStatus());
            stepService.updateById(step);
            return step;
        }

        for (NodeDto nodeDto : nodeDtoList) {
            String nodeName = nodeDto.getNodeName();
            if (nodeDto.getNodeDevices().isEmpty()) {
                continue;
            }
            for (Device nodeDevice : nodeDto.getNodeDevices()) {
                Integer osdId = nodeDevice.getOsdId();
                try {
                    RemoteResponse response = osdApi.updateOsdBucketRequest(RemoteCallUtil.generateRemoteRequest(), osdId, CrushFailureDomainEnum.HOST, nodeName);
                    boolean flag = true;//use to check whether the task in cluster is complete
                    while (flag) {
                        step = getCrushmapStepResult.getStepResponse(step, response, "", ResultCode.POOL_DELETE_ERROR.getMessage());
                        if (Objects.equals(step.getStepStatus(), TaskStatusEnum.FINISH.getStatus()) || Objects.equals(step.getStepStatus(), TaskStatusEnum.FAIL.getStatus())) {
                            flag = false;
                        }
                    }
                } catch (Throwable e) {
                    step.setStepStatus(TaskStatusEnum.FAIL.getStatus());
                }
            }
        }
        if (!Objects.equals(step.getStepStatus(), TaskStatusEnum.FAIL.getStatus())) {
            step.setStepStatus(TaskStatusEnum.FINISH.getStatus());
            stepService.updateById(step);
        }
        return step;
    }

    private Step deletePoolBucket(String poolName, Step step) {
        //update step status to executing
        step.setStepStatus(TaskStatusEnum.EXECUTING.getStatus());
        try {
            //get all bucket
            CrushmapBucket crushmapBuckets = crushmapApi.getCrushmapBuckets(RemoteCallUtil.generateRemoteRequest());
            //delete pool's all host bucket
            for (CrushmapBucket.BucketsDTO bucket : crushmapBuckets.getBuckets()) {
                //host bucketName construct with poolName + BUCKET_NAME_CONNECTOR("_") + nodeName. example:pool1_node1
                //ceph also create a bucket named bucketName + AUTO_GENERATED_BUCKET_NAME_CONNECTOR("~") + "hdd"/"ssd", it's useless for dsms-engine,so we should ignore them
                if (bucket.getName().startsWith(poolName + CrushmapConst.BUCKET_NAME_CONNECTOR) && !bucket.getName().contains(CrushmapConst.AUTO_GENERATED_BUCKET_NAME_CONNECTOR)) {
                    RemoteResponse response = crushmapApi.delBucket(RemoteCallUtil.generateRemoteRequest(), bucket.getName());
                    Step deleteHostBucketStep = getCrushmapStepResult.getStepResponse(step, response, DelBucketRequest.DELETE_BUCKET_SUCCESS, ResultCode.POOL_DELETE_ERROR.getMessage());
                    if (!Objects.equals(deleteHostBucketStep.getStepStatus(), TaskStatusEnum.FINISH.getStatus())) {
                        throw new Throwable(deleteHostBucketStep.getStepErrorMessage());
                    }
                    step.setStepStatus(TaskStatusEnum.EXECUTING.getStatus());
                }
            }
            //root bucket can be deleted only after host bucket is all deleted
            RemoteResponse response = crushmapApi.delBucket(RemoteCallUtil.generateRemoteRequest(), poolName);
            Step deleteRootBucketStep = getCrushmapStepResult.getStepResponse(step, response, DelBucketRequest.DELETE_BUCKET_SUCCESS, ResultCode.POOL_DELETE_ERROR.getMessage());
            if (!Objects.equals(deleteRootBucketStep.getStepStatus(), TaskStatusEnum.FINISH.getStatus())) {
                throw new Throwable(deleteRootBucketStep.getStepErrorMessage());
            }
        } catch (Throwable e) {
            step.setStepStatus(TaskStatusEnum.FAIL.getStatus());
        }
        return step;
    }

    private Step deleteEcProfile(String poolName, Step step) {
        //update step status to executing
        step.setStepStatus(TaskStatusEnum.EXECUTING.getStatus());
        try {
            RemoteResponse deleteEcProfileResponse = ecProfileApi.deleteEcProfile(RemoteCallUtil.generateRemoteRequest(), poolName);
            boolean flag = true;//use to check whether the task in cluster is complete
            while (flag) {
                step = getCrushmapStepResult.getStepResponse(step, deleteEcProfileResponse, "", ResultCode.POOL_DELETE_ERROR.getMessage());
                if (Objects.equals(step.getStepStatus(), TaskStatusEnum.FINISH.getStatus()) || Objects.equals(step.getStepStatus(), TaskStatusEnum.FAIL.getStatus())) {
                    flag = false;
                }
            }
        } catch (Throwable e) {
            step.setStepStatus(TaskStatusEnum.FAIL.getStatus());
        }
        return step;
    }

    @Override
    public boolean validateTask(String[] validateParam) {
        return taskService.validateTaskMessageAndTaskType(validateParam[0]);
    }

}

