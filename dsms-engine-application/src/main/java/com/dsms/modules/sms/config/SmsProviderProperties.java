/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.sms.config;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

/**
 * Sms provider properties
 */
@Data
@Validated
public class SmsProviderProperties {

    /**
     * sms provider name
     */
    @NotNull(message = "短信服务商不能为空")
    private String smsProvider;

    /**
     * sms provider host
     */
    @NotNull(message = "短信服务商域名不能为空")
    private String smsProviderHost;

    /**
     * sms region id
     */
    private String smsRegion;

    /**
     * sms provider access id
     */
    @NotEmpty(message = "短信平台访问账号不能为空")
    private String smsAccessId;

    /**
     * sms provider access secret
     */
    @NotEmpty(message = "短信平台访问密钥不能为空")
    private String smsAccessSecret;

    /**
     * sms provider tencent sdk app id
     */
    private String smsAppId;

    /**
     * sms provider use proxy
     */
    private boolean proxyEnabled;

    /**
     * sms provider request proxy scheme
     */
    private String proxyScheme;

    /**
     * sms provider request proxy host
     */
    private String proxyHost;

    /**
     * sms provider request proxy port
     */
    private int proxyPort;

}
