package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.DeviceStatusEnum;
import org.junit.jupiter.api.Test;


class DeviceStatusEnumTest {


    @Test
    void testGetters() {
        DeviceStatusEnum available = DeviceStatusEnum.AVAILABLE;
        assertEquals(1, available.getStatus());
        assertEquals("可用", available.getMessage());
    }

}
