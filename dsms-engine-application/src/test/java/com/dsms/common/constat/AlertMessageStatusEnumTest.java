package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.AlertMessageStatusEnum;
import org.junit.jupiter.api.Test;

class AlertMessageStatusEnumTest {

    @Test
    void testGetters() {
        AlertMessageStatusEnum unConfirm = AlertMessageStatusEnum.UN_CONFIRM;
        assertEquals(0, unConfirm.getStatus().intValue());
        assertEquals("未读", unConfirm.getMessage());

        AlertMessageStatusEnum confirm = AlertMessageStatusEnum.CONFIRM;
        assertEquals(1, confirm.getStatus().intValue());
        assertEquals("已读", confirm.getMessage());
    }

}
