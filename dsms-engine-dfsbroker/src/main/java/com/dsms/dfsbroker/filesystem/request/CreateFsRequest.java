/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.filesystem.request;

import cn.hutool.core.lang.Validator;
import com.alibaba.fastjson2.JSON;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import com.dsms.dfsbroker.filesystem.model.dto.FileSystemDTO;
import org.springframework.http.HttpMethod;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.Map;

public class CreateFsRequest extends RemoteRequest {

    public static final String CREATE_FS_SUCCESS = "new fs with";

    public CreateFsRequest(FileSystemDTO fileSystem) {
        if (ObjectUtils.isEmpty(fileSystem.getFsName()) || ObjectUtils.isEmpty(fileSystem.getMetadata()) || ObjectUtils.isEmpty(fileSystem.getData())) {
            throw new IllegalArgumentException("fsName or metadata or data must not be empty");
        }
        if (!Validator.isGeneral(fileSystem.getFsName()) || !Validator.isGeneral(fileSystem.getMetadata()) || !Validator.isGeneral(fileSystem.getData()) ) {
            throw new IllegalArgumentException("fsName or metadata or data contains illegal characters");
        }
        Map<String, Object> requestParam = new HashMap<>(4);
        requestParam.put("prefix", "fs new");
        requestParam.put("fs_name", fileSystem.getFsName());
        requestParam.put("metadata", fileSystem.getMetadata());
        requestParam.put("data", fileSystem.getData());
        requestParam.put("force", true);

        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}
