package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.OdsStatusEnum;
import org.junit.jupiter.api.Test;


class OdsStatusEnumTest {


    @Test
    void testGetters() {
        OdsStatusEnum up = OdsStatusEnum.UP;
        assertEquals("up", up.getStatus());
    }

}
