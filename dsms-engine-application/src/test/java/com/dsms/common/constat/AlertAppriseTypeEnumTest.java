package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.dsms.common.constant.AlertAppriseTypeEnum;
import com.dsms.common.constant.AlertSmsProviderEnum;
import org.junit.jupiter.api.Test;

class AlertAppriseTypeEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(AlertAppriseTypeEnum.SMS, AlertAppriseTypeEnum.getByCode(1) );
    }

    @Test
    void testGetWrongCode() {
        assertNull(AlertAppriseTypeEnum.getByCode(3));
    }

    @Test
    void testGetters() {
        AlertAppriseTypeEnum email = AlertAppriseTypeEnum.EMAIL;
        assertEquals(0, email.getCode());
        assertEquals("邮件", email.getName());
    }
}
