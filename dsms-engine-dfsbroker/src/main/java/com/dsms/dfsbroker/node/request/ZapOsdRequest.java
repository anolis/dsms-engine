/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.node.request;

import com.alibaba.fastjson2.JSON;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import org.springframework.http.HttpMethod;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.Map;

public class ZapOsdRequest extends RemoteRequest {
    public static final int RETRY_TIMES = 3;
    public static final int RETRY_TIME_SECONDS = 5;
    public static final String ZAP_SUCCESSFUL_RESULT = "Zapping successful for: <Raw Device: %s>";

    public ZapOsdRequest(String hostname, String devicePath) {
        if (ObjectUtils.isEmpty(hostname) || ObjectUtils.isEmpty(devicePath)) {
            throw new IllegalArgumentException("hostname or devicePath must not be empty");
        }
        Map<String, Object> requestParam = new HashMap<>(4);
        requestParam.put("prefix", "orch device zap");
        requestParam.put("hostname", hostname);
        requestParam.put("path", devicePath);
        requestParam.put("force", true);
        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}
