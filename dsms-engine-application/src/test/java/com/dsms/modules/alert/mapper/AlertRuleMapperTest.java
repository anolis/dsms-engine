/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.mapper;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.dsms.modules.alert.entity.AlertRule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AlertRuleMapperTest {
    @Autowired
    private AlertRuleMapper alertRuleMapper;


    @Test
    @DisplayName("alert-rule mapper crud")
    @Transactional
    void testNormal() {
        // mock a task bean
        AlertRule alertRule = new AlertRule();
        alertRule.setRuleMetric("test");
        alertRule.setRuleLevel(1);
        // assert create feature
        int result = alertRuleMapper.insert(alertRule);
        Assertions.assertNotEquals(0, result);
        AlertRule alertRuleEntity = alertRuleMapper.selectById(alertRule.getId());
        assertNotNull(alertRuleEntity);

        // assert update feature
        alertRuleEntity.setRuleMetric("new_name");
        int updateResult = alertRuleMapper.updateById(alertRuleEntity);
        Assertions.assertNotEquals(0, updateResult);
        assertEquals(alertRule.getId(), alertRuleEntity.getId());
        AlertRule alertRuleNew = alertRuleMapper.selectById(alertRuleEntity.getId());
        assertEquals(alertRuleNew.getRuleMetric(), alertRuleEntity.getRuleMetric());

        // assert delete feature
        alertRuleMapper.deleteById(alertRuleEntity.getId());
        AlertRule alertRuleToDelete = alertRuleMapper.selectById(alertRuleEntity.getId());
        assertNull(alertRuleToDelete);
    }
}