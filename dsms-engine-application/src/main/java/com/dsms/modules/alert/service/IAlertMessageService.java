/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dsms.common.constant.AlertRuleLevelEnum;
import com.dsms.common.constant.AlertRuleModuleEnum;
import com.dsms.modules.alert.entity.AlertMessage;
import com.dsms.modules.alert.model.vo.AlertMessageManageVO;
import com.dsms.modules.alert.model.vo.AlertMessagePageVO;
import com.dsms.modules.alert.model.vo.AlertMessageQueryVO;

import java.util.List;

/**
 * AlertMessage operation service
 */
public interface IAlertMessageService extends IService<AlertMessage> {
    /**
     * lists alert messages by the specified query conditions
     * the list is sorted by time and status
     *
     * @param alertMessagePageVO
     * @return Page<AlertMessage>
     */
    Page<AlertMessage> listAlertMessage(AlertMessagePageVO alertMessagePageVO);

    /**
     * get single alert message by query condition
     *
     * @param alertMessageQueryVO
     * @return AlertMessage
     */
    AlertMessage getAlertMessage(AlertMessageQueryVO alertMessageQueryVO);

    /**
     * batch confirmation of alarm message
     *
     * @param alertMessageManageVO
     * @return Boolean
     */
    boolean confirmMessage(AlertMessageManageVO alertMessageManageVO);

    /**
     * batch deletion of alarm message
     *
     * @param alertMessageManageVO
     * @return Boolean
     */
    boolean deleteMessage(AlertMessageManageVO alertMessageManageVO);

    /**
     * get the number of unconfirmed alert message
     *
     * @return Integer
     */
    long getUnconfirmedMessageNum();

    /**
     * get the list of alert message which not send
     *
     * @return List<AlertMessage>
     */
    List<AlertMessage> getUnSendMessages();

    /**
     * convert custom message to system module alert message
     *
     * @param message             custom message
     * @param alertRuleModuleEnum alert rule module
     * @param alertRuleLevelEnum  alert rule level
     * @return alert message
     */
    AlertMessage convertCustomMessage(String message, AlertRuleModuleEnum alertRuleModuleEnum, AlertRuleLevelEnum alertRuleLevelEnum);
}
