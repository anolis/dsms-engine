/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.dfsbroker.cluster.api;


import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.dsms.common.constant.ResultCode;
import com.dsms.common.exception.DsmsEngineException;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.common.remotecall.service.IRemoteCall;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.dfsbroker.cluster.model.remote.ServerResult;
import com.dsms.dfsbroker.cluster.model.remote.StatusResult;
import com.dsms.dfsbroker.common.CommandDirector;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.dfsbroker.node.model.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ClusterApi {
    public static final int MAX_RETRIES = CommonApi.DEFAULT_RETRY_TIMES;

    private final IRemoteCall remoteCall;

    private final CommonApi commonApi;

    @Autowired
    public ClusterApi(IRemoteCall remoteCall, CommonApi commonApi) {
        this.remoteCall = remoteCall;
        this.commonApi = commonApi;
    }

    /**
     * get dsms-storage nodes info
     * @return node info list
     */
    public Cluster getClusterVersion(RemoteRequest remoteRequest) {
        Cluster cluster = new Cluster();
        Node[] version;

        remoteRequest.setUrlPrefix("server");
        remoteRequest.setHttpMethod(HttpMethod.GET);
        RemoteResponse remoteResponse;
        try {
            remoteResponse = remoteCall.callRestfulApi(remoteRequest);
        } catch (AuthenticationException e) {
            throw new DsmsEngineException(e, ResultCode.CLUSTER_KEY_ERROR);
        } catch (Throwable e) {
            throw new DsmsEngineException(e, ResultCode.REMOTE_CALL_FAIL);
        }
        String message = remoteResponse.getBody();
        JSONArray objects = JSONUtil.parseArray(message);
        version = new Node[objects.size()];
        List<JSONObject> jsonObjects = objects.toList(JSONObject.class);
        for (int i = 0; i < jsonObjects.size(); i++) {
            Node node = new Node();

            String cephVersion = jsonObjects.get(i).getStr("ceph_version");
            node.setVersion(cephVersion);
            version[i] = node;
        }
        cluster.setNodes(version);

        return cluster;
    }

    public List<ServerResult> getServer(RemoteRequest remoteRequest) throws Throwable {
        CommandDirector.constructServerRequest(remoteRequest);
        ServerResult[] responseMon = commonApi.remoteCall(remoteRequest, MAX_RETRIES, ServerResult[].class);
        return Arrays.stream(responseMon).collect(Collectors.toList());

    }

    public StatusResult getStatus(RemoteRequest remoteRequest) throws Throwable {
        CommandDirector.constructStatusRequest(remoteRequest);
        return commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, StatusResult.class);

    }
}
