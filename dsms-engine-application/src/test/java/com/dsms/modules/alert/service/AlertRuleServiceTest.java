/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.service;


import com.dsms.modules.alert.entity.AlertRule;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AlertRuleServiceTest {
    @Autowired
    private IAlertRuleService alertRuleService;


    @Test
    @DisplayName("alert-rule crud")
    @Transactional
    void testNormal() {
        // mock a task bean
        AlertRule alertRule = new AlertRule();
        alertRule.setRuleMetric("test");
        alertRule.setRuleLevel(1);
        // assert create feature
        boolean result = alertRuleService.save(alertRule);
        assertTrue(result);
        AlertRule alertRuleEntity = alertRuleService.getById(alertRule.getId());
        assertNotNull(alertRuleEntity);

        // assert update feature
        alertRuleEntity.setRuleMetric("new_name");
        boolean result1 = alertRuleService.updateById(alertRuleEntity);
        assertTrue(result1);
        assertEquals(alertRule.getId(), alertRuleEntity.getId());
        AlertRule alertRuleNew = alertRuleService.getById(alertRuleEntity.getId());
        assertEquals(alertRuleNew.getRuleMetric(), alertRuleEntity.getRuleMetric());

        // assert delete feature
        alertRuleService.removeById(alertRuleEntity.getId());
        AlertRule alertRuleToDelete = alertRuleService.getById(alertRuleEntity.getId());
        assertNull(alertRuleToDelete);
    }
}