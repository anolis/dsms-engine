package com.dsms.common.util;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.dsms.common.taskmanager.model.Task;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

class ObjectUtilTest {

    @SneakyThrows
    @Test
    void testObjectNullOrHasNull() {
        Task taskIsNull = null;

        Task taskHasNullField = new Task();
        taskHasNullField.setId(1);

        Task task2HasBlankString = new Task();
        task2HasBlankString.setId(1);
        task2HasBlankString.setTaskName("");

        assertTrue(ObjectUtil.checkObjFieldContainNull(taskHasNullField));
        assertTrue(ObjectUtil.checkObjFieldContainNull(taskIsNull));
        assertTrue(ObjectUtil.checkObjFieldContainNull(task2HasBlankString));
    }
}