/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.common;

import com.dsms.common.constant.CrushFailureDomainEnum;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.auth.model.AuthDTO;
import com.dsms.dfsbroker.auth.request.AuthGetKeyRequest;
import com.dsms.dfsbroker.cluster.request.ServerRequest;
import com.dsms.dfsbroker.cluster.request.StatusRequest;
import com.dsms.dfsbroker.common.request.GetResultRequest;
import com.dsms.dfsbroker.filesystem.model.dto.FileSystemDTO;
import com.dsms.dfsbroker.filesystem.request.*;
import com.dsms.dfsbroker.node.request.*;
import com.dsms.dfsbroker.osd.crushmap.request.*;
import com.dsms.dfsbroker.osd.ecprofile.model.dto.EcProfileDto;
import com.dsms.dfsbroker.osd.ecprofile.request.CreateEcProfileRequest;
import com.dsms.dfsbroker.osd.ecprofile.request.DeleteEcProfileRequest;
import com.dsms.dfsbroker.osd.ecprofile.request.GetEcProfileRequest;
import com.dsms.dfsbroker.osd.osd.request.GetOsdByIdRequest;
import com.dsms.dfsbroker.osd.osd.request.GetOsdMetadataRequest;
import com.dsms.dfsbroker.osd.osd.request.ListOsdStatusRequest;
import com.dsms.dfsbroker.osd.osd.request.UpdateOsdBucketRequest;
import com.dsms.dfsbroker.rbd.request.EnableRbdMonRequest;
import com.dsms.dfsbroker.rbd.request.GetEnabledRbdMonPoolRequest;
import com.dsms.dfsbroker.storagedir.model.dto.StorageDirDTO;
import com.dsms.dfsbroker.storagedir.request.*;
import com.dsms.dfsbroker.storagepool.model.dto.StoragePoolCreateDTO;
import com.dsms.dfsbroker.storagepool.request.*;

public class CommandDirector {

    private CommandDirector() {
    }
    public static void constructServerRequest(RemoteRequest request) {
        request.buildRequestParam(new ServerRequest());
    }

    public static void constructStatusRequest(RemoteRequest request) {
        request.buildRequestParam(new StatusRequest());
    }

    public static void constructMonRequest(RemoteRequest request) {
        request.buildRequestParam(new MonRequest());
    }

    public static void constructOrchDeviceLsRequest(RemoteRequest request, String hostname) {
        request.buildRequestParam(new OrchDeviceLsRequest(hostname));
    }

    public static void constructOsdDfRequest(RemoteRequest request) {
        request.buildRequestParam(new OsdDfRequest());
    }

    public static void constructAddOsd(RemoteRequest request, String nodeName, String devicePath) {
        request.buildRequestParam(new OsdAddRequest(nodeName, devicePath));
    }

    public static void constructGetRequest(RemoteRequest request, String requestId) {
        request.buildRequestParam(new GetResultRequest(requestId));
    }

    public static void constructRmOsd(RemoteRequest request, String[] osdId) {
        request.buildRequestParam(new RmOsdRequest(osdId));
    }

    public static void constructRmOsdStatus(RemoteRequest request) {
        request.buildRequestParam(new RmOsdStatusRequest());
    }

    public static void constructZapOsd(RemoteRequest request, String host, String devicePath) {
        request.buildRequestParam(new ZapOsdRequest(host, devicePath));
    }

    public static void constructStopOsd(RemoteRequest request, Integer osdId) {
        request.buildRequestParam(new StopOsdRequest(osdId));
    }

    public static void constructPurgeOsd(RemoteRequest request, Integer osdId) {
        request.buildRequestParam(new PurgeOsdRequest(osdId));
    }

    public static void constructGetStoragePoolByPoolIdRequest(RemoteRequest request, int id) {
        request.buildRequestParam(new GetStoragePoolByPoolIdRequest(id));
    }

    public static void constructStoragePoolLsRequest(RemoteRequest request) {
        request.buildRequestParam(new StoragePoolLsRequest());
    }

    public static void constructGetDfRequest(RemoteRequest request) {
        request.buildRequestParam(new DfRequest());
    }

    public static void constructFsLsRequest(RemoteRequest request) {
        request.buildRequestParam(new FsLsRequest());
    }

    public static void constructCreateBucketRequest(RemoteRequest request, String bucketName, int type, String root) {
        request.buildRequestParam(new CreateBucketRequest(bucketName, type, root));
    }

    public static void constructCreateRuleRequest(RemoteRequest request, String ruleName) {
        request.buildRequestParam(new CreateRuleRequest(ruleName));
    }

    public static void constructCreateEcProfileRequest(RemoteRequest request, EcProfileDto ecProfile) {
        request.buildRequestParam(new CreateEcProfileRequest(ecProfile));
    }

    public static void constructGetEcProfileRequest(RemoteRequest request, String ecProfileName) {
        request.buildRequestParam(new GetEcProfileRequest(ecProfileName));
    }

    public static void constructDeleteEcProfileRequest(RemoteRequest request, String ecProfileName) {
        request.buildRequestParam(new DeleteEcProfileRequest(ecProfileName));
    }

    public static void constructCreatePoolRequest(RemoteRequest request, StoragePoolCreateDTO pool) {
        request.buildRequestParam(new CreateStoragePoolRequest(pool));
    }

    public static void constructDeletePoolRequest(RemoteRequest request, String poolName) {
        request.buildRequestParam(new DeleteStoragePoolRequest(poolName));
    }

    public static void constructAllowEcOverWritesRequest(RemoteRequest request, String poolName) {
        request.buildRequestParam(new AllowEcOverWrites(poolName));
    }

    public static void constructGetOSDById(RemoteRequest request, int id) {
        request.buildRequestParam(new GetOsdByIdRequest(id));
    }

    public static void constructListOSDStatus(RemoteRequest request) {
        request.buildRequestParam(new ListOsdStatusRequest());
    }

    public static void constructOsdMetadataRequest(RemoteRequest request) {
        request.buildRequestParam(new GetOsdMetadataRequest());
    }

    public static void constructGetCrushmapBucketsRequest(RemoteRequest request) {
        request.buildRequestParam(new GetBucketRequest());
    }

    public static void constructDelBucketRequest(RemoteRequest request, String bucketName) {
        request.buildRequestParam(new DelBucketRequest(bucketName));
    }

    public static void constructUpdateOsdBucketRequest(RemoteRequest request, int osdid, CrushFailureDomainEnum crushFailureDomainEnum, String targetBucket) {
        request.buildRequestParam(new UpdateOsdBucketRequest(osdid, crushFailureDomainEnum, targetBucket));
    }

    public static void constructDelRuleRequest(RemoteRequest request, String ruleName) {
        request.buildRequestParam(new DelCrushRuleRequest(ruleName));
    }

    public static void constructFsStatusRequest(RemoteRequest request) {
        request.buildRequestParam(new FsStatusRequest());
    }

    public static void constructCreateFsRequest(RemoteRequest request, FileSystemDTO fileSystem) {
        request.buildRequestParam(new CreateFsRequest(fileSystem));
    }

    public static void constructRemoveFsRequest(RemoteRequest request, FileSystemDTO fileSystem) {
        request.buildRequestParam(new RemoveFsRequest(fileSystem));
    }

    public static void constructFailFsRequest(RemoteRequest request, FileSystemDTO fileSystem) {
        request.buildRequestParam(new FailFsRequest(fileSystem));
    }

    public static void constructStorageDirLsRequest(RemoteRequest request, StorageDirDTO storageDirDTO) {
        request.buildRequestParam(new StorageDirLsRequest(storageDirDTO));
    }

    public static void constructStorageDirInfoRequest(RemoteRequest request, StorageDirDTO storageDirDTO) {
        request.buildRequestParam(new StorageDirInfoRequest(storageDirDTO));
    }

    public static void constructStorageDirAuthListRequest(RemoteRequest request, StorageDirDTO storageDirDTO) {
        request.buildRequestParam(new StorageDirAuthListRequest(storageDirDTO));
    }

    public static void constructAuthGetKeyRequest(RemoteRequest request, AuthDTO authDTO) {
        request.buildRequestParam(new AuthGetKeyRequest(authDTO));
    }

    public static void constructStorageDirAuthorizeRequest(RemoteRequest request, StorageDirDTO storageDirDTO) {
        request.buildRequestParam(new StorageDirAuthRequest(storageDirDTO));
    }

    public static void constructCreateStorageDirRequest(RemoteRequest request, StorageDirDTO storageDirDTO) {
        request.buildRequestParam(new CreateStorageDirRequest(storageDirDTO));


    }

    public static void constructStorageDirDeAuthorizeRequest(RemoteRequest request, StorageDirDTO storageDirDTO) {
        request.buildRequestParam(new StorageDirDeAuthRequest(storageDirDTO));
    }

    public static void constructRemoveStorageDirRequest(RemoteRequest request, StorageDirDTO storageDirDTO) {
        request.buildRequestParam(new RemoveStorageDirRequest(storageDirDTO));
    }

    public static void constructGetEnabledRbdMonPoolRequest(RemoteRequest request) {
        request.buildRequestParam(new GetEnabledRbdMonPoolRequest());
    }

    public static void constructEnableRbdMonRequest(RemoteRequest request, String poolName) {
        request.buildRequestParam(new EnableRbdMonRequest(poolName));
    }
}
