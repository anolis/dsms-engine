package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.dsms.common.constant.CrushFailureDomainEnum;
import org.junit.jupiter.api.Test;

class CrushFailureDomainEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(CrushFailureDomainEnum.HOST.getType(), CrushFailureDomainEnum.getCrushFailureDomain(1));
    }

    @Test
    void testGetWrongCode() {
        assertThrows(IllegalArgumentException.class, () -> CrushFailureDomainEnum.getCrushFailureDomain(100));
    }
}
