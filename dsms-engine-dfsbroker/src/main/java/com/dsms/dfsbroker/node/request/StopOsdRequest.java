/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.node.request;

import com.alibaba.fastjson2.JSON;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpMethod;
import org.springframework.util.ObjectUtils;

public class StopOsdRequest extends RemoteRequest {
    public static final String OSD_STOP_SUCCESS_RESULT = "stop osd.%s. ";
    public static final String OSD_ALREADY_STOP_SUCCESS_RESULT = "osd.%s is already stopped. ";
    public static final String OSD_NOT_EXIST_SUCCESS_RESULT = "osd.%s does not exist. ";

    public StopOsdRequest(Integer osdId) {
        if (ObjectUtils.isEmpty(osdId)) {
            throw new IllegalArgumentException("osdId must not be empty");
        }
        Map<String, Object> requestParam = new HashMap<>(4);
        requestParam.put("prefix", "osd stop");
        requestParam.put("ids", new String[]{
            String.valueOf(osdId)
        });
        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}
