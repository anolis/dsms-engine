package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.RbdDeviceTypeEnum;
import org.junit.jupiter.api.Test;

class RbdDeviceTypeEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(Boolean.TRUE, RbdDeviceTypeEnum.isRbdDeviceType("/dev/rbd1"));
        assertEquals(Boolean.TRUE, RbdDeviceTypeEnum.isRbdDeviceType("/dev/nbd1"));
    }

    @Test
    void testGetWrongCode() {
        assertEquals(Boolean.FALSE, RbdDeviceTypeEnum.isRbdDeviceType("/dev/sda"));
    }
}
