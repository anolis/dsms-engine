package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.CommonEnum;
import org.junit.jupiter.api.Test;


class CommonEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(1, CommonEnum.ENABLE.getCode());
    }

    @Test
    void testGetters() {
        CommonEnum disable = CommonEnum.DISABLE;
        assertEquals(0, disable.getCode());
        assertEquals("关闭", disable.getName());
    }

}
