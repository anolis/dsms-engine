package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.AlertRuleModuleEnum;
import com.dsms.common.constant.AlertRuleTypeEnum;
import org.junit.jupiter.api.Test;

class AlertRuleModuleEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(AlertRuleModuleEnum.CLUSTER, AlertRuleModuleEnum.getAlertRuleModuleEnum("cluster"));
    }

    @Test
    void testGetWrongCode() {
        assertEquals(AlertRuleModuleEnum.DEFAULT, AlertRuleModuleEnum.getAlertRuleModuleEnum("test"));
    }

    @Test
    void testGetters() {
        AlertRuleModuleEnum cluster = AlertRuleModuleEnum.CLUSTER;
        assertEquals("集群", cluster.getModuleName());
        assertEquals("cluster", cluster.getModuleCode());
        assertEquals("集群 ${metric} 指标 ${compare} ${threshold}, 请及时处理", cluster.getModuleAlertTemplate());
    }
}
