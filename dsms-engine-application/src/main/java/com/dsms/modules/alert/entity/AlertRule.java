/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;

import java.io.Serializable;


@Data
@TableName("alert_rule")
@ApiModel(value = "AlertRule对象", description = "告警规则表")
public class AlertRule implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("告警规则id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("告警规则名称")
    @TableField(exist = false)
    private String ruleName;

    @ApiModelProperty("告警规则监控项")
    private String ruleMetric;

    @ApiModelProperty("告警规则表达式")
    @JsonIgnore
    private String rulePromql;

    @ApiModelProperty("告警规则比较运算符（0-相等，1-大于，-1-小于，2-不相等）")
    private Integer ruleCompareType;

    @ApiModelProperty("告警规则阈值")
    private String ruleThreshold;

    @ApiModelProperty("告警规则阈值初始值")
    @JsonIgnore
    private String ruleThresholdInit;

    @ApiModelProperty("告警规则周期")
    private Integer ruleTimes;

    @ApiModelProperty("告警规则周期初始值")
    @JsonIgnore
    private Integer ruleTimesInit;

    @ApiModelProperty("告警规则模块")
    private String ruleModule;

    @ApiModelProperty("告警级别（0-提示、1-一般、2-重要、3-紧急）")
    private Integer ruleLevel;

    @ApiModelProperty("告警级别初始值")
    @JsonIgnore
    private Integer ruleLevelInit;

    @ApiModelProperty("告警规则检测时间")
    private LocalDateTime lastCheckTime;


}
