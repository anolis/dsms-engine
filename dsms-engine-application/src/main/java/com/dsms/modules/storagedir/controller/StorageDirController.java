/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.storagedir.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.model.Result;
import com.dsms.dfsbroker.storagedir.model.StorageDir;
import com.dsms.dfsbroker.storagedir.model.dto.StorageDirDTO;
import com.dsms.dfsbroker.storagedir.service.IStorageDirService;
import com.dsms.modules.storagedir.model.vo.StorageDirCreateVO;
import com.dsms.modules.storagedir.model.vo.StorageDirDeleteVO;
import com.dsms.modules.storagedir.model.vo.StorageDirPageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/storagedir")
@Api(tags = "存储目录模块")
public class StorageDirController {

    @Autowired
    private IStorageDirService storageDirService;

    @ApiOperation("存储目录模块-获取存储目录信息")
    @PostMapping("/list")
    public Result<Page<StorageDir>> list(@Validated @RequestBody StorageDirPageVO storageDirPageVO) {
        Page<StorageDir> storageDirList = storageDirService.page(storageDirPageVO);

        return Result.OK(storageDirList);
    }

    @ApiOperation("存储目录模块-创建存储目录")
    @PostMapping("/create_dir")
    public Result<Boolean> createDir(@Validated @RequestBody StorageDirCreateVO storageDirCreateVO) {
        StorageDirDTO storageDirDTO = new StorageDirDTO(storageDirCreateVO.getFsName(), storageDirCreateVO.getStorageDirName(), storageDirCreateVO.getStorageDirName());

        return Result.OK(storageDirService.createStorageDir(storageDirDTO));
    }

    @ApiOperation("存储目录模块-删除存储目录")
    @PostMapping("/delete_dir")
    public Result<Boolean> deleteDir(@Validated @RequestBody StorageDirDeleteVO storageDirDeleteVO) {
        StorageDirDTO storageDirDTO = new StorageDirDTO(storageDirDeleteVO.getFsName(), storageDirDeleteVO.getStorageDirName());

        return Result.OK(storageDirService.deleteStorageDir(storageDirDTO));
    }

}
