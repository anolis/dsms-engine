package com.dsms.common.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class FilterUtilTest {

    @Test
    public void testMatches() {
        String actual = "test";
        String actualWrong = "test1";
        String expected = "test";
        assertTrue(FilterUtil.matches(expected, actual));
        assertFalse(FilterUtil.matches(expected, actualWrong));
    }

    @Test
    public void testMatchesNull() {
        assertTrue(FilterUtil.matches(null, "test"));
    }

    @Test
    public void testFuzzy() {
        String actual = "test123";
        String actualWrong = "te23st12";
        String expected = "test";
        assertTrue(FilterUtil.fuzzy(expected, actual));
        assertFalse(FilterUtil.fuzzy(expected, actualWrong));
    }

    @Test
    public void testFuzzyBlank() {
        assertTrue(FilterUtil.fuzzy("", "test"));
    }

}