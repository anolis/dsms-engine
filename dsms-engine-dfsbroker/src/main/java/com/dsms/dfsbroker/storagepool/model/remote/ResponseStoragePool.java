/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagepool.model.remote;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@Data
@ToString
public class ResponseStoragePool {
    @JSONField(name = "pool")
    private int poolId;

    @JSONField(name = "pool_name")
    private String poolName;

    @JSONField(name = "pg_num")
    private int pgNum;

    @JSONField(name = "pgp_num")
    private int pgpNum;

    //when the storage pool can work normally,the number of osds that must be used in compliance with the regulations (failure domains)
    //the replica pool is the number of copies, and the erasure pool is the sum of k and m
    //TODO the meaning of "size" will cause ambiguity, the return field of dsms-storage should be changed
    @JSONField(name = "size")
    private int crushOsdNum;

    //the type of storage: 1 represents the replica pool, 3 represents the erasure pool
    @JSONField(name = "type")
    private int type;
}
