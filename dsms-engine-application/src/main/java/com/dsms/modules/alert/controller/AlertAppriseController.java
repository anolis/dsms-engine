/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.constant.AlertAppriseTypeEnum;
import com.dsms.common.model.PageParam;
import com.dsms.common.model.Result;
import com.dsms.modules.alert.entity.AlertApprise;
import com.dsms.modules.alert.entity.AlertContact;
import com.dsms.modules.alert.service.IAlertAppriseService;
import com.dsms.modules.alert.service.IAlertContactService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/alertapprise")
@Api(tags = "告警通知模块")
public class AlertAppriseController {

    private final IAlertAppriseService alertAppriseService;
    private final IAlertContactService alertContactService;

    @Autowired
    public AlertAppriseController (IAlertAppriseService alertAppriseService, IAlertContactService alertContactService) {
        this.alertAppriseService = alertAppriseService;
        this.alertContactService = alertContactService;
    }


    @ApiOperation("告警通知模块-获取告警通知配置列表")
    @PostMapping("/list")
    public Result<Page<AlertApprise>> list(@Validated @RequestBody PageParam pageParam) {
        Page<AlertApprise> alertApprisePage = alertAppriseService.page(new Page<>(pageParam.getPageNo(), pageParam.getPageSize()));
        List<AlertContact> alertContacts = alertContactService.list();
        Map<Integer, List<AlertContact>> alertContactsGroup = alertContacts.stream().collect(Collectors.groupingBy(AlertContact::getContactType));
        List<AlertApprise> alertAppriseList = new ArrayList<>();

        for (AlertAppriseTypeEnum value : AlertAppriseTypeEnum.values()) {
            alertApprisePage.getRecords()
                .stream()
                .filter(alertApprise -> alertApprise.getType().equals(value.getCode()))
                .findAny()
                .orElseGet(() -> {
                    AlertApprise newAlertApprise = new AlertApprise();
                    newAlertApprise.setType(value.getCode());
                    alertAppriseList.add(newAlertApprise);
                    return newAlertApprise;
                })
                .setContacts(alertContactsGroup.get(value.getCode()));
        }
        alertAppriseList.addAll(alertApprisePage.getRecords());
        alertApprisePage.setRecords(alertAppriseList);

        return Result.OK(alertApprisePage);
    }

    @ApiOperation("告警通知模块-更新告警通知配置")
    @PostMapping("/update_apprise")
    public Result<Boolean> updateApprise(@Validated @RequestBody AlertApprise alertApprise) {

        return Result.OK(alertAppriseService.saveOrUpdateAlertApprise(alertApprise));
    }


    @ApiOperation("告警通知模块-发送测试邮件")
    @PostMapping("/send_email")
    public Result<Boolean> sendTestEmail() {
        return Result.OK(alertAppriseService.sendTestEmail());
    }


    @ApiOperation("告警通知模块-发送测试短信")
    @PostMapping("/send_sms")
    public Result<Boolean> sendTestSms() {
        return Result.OK(alertAppriseService.sendTestSms());
    }

}
