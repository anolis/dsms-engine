package com.dsms.common.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ByteUtilTest {

    @Test
    public void testGbToBytes() {
        double input = 1.0;
        double expected = 1024D * 1024 * 1024;
        assertEquals(expected, ByteUtil.gbToBytes(input));
    }

    @Test
    public void testGbToBytesWith0() {
        double input = 0.0;
        double expected = 0;
        assertEquals(expected, ByteUtil.gbToBytes(input));
    }

    @Test
    public void testGbToBytesWithDecimalValue() {
        double input = 0.5;
        double expected = 512D * 1024 * 1024;
        assertEquals(expected, ByteUtil.gbToBytes(input));
    }

}