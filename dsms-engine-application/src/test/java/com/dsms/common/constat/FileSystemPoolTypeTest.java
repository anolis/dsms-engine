package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.FileSystemPoolTypeEnum;
import org.junit.jupiter.api.Test;


class FileSystemPoolTypeTest {


    @Test
    void testGetters() {
        FileSystemPoolTypeEnum data = FileSystemPoolTypeEnum.DATA;
        assertEquals("data", data.getMessage());
    }

}
