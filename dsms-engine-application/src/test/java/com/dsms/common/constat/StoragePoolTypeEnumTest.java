package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.StoragePoolTypeEnum;
import org.junit.jupiter.api.Test;

class StoragePoolTypeEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(StoragePoolTypeEnum.REPLICATED.getType(), StoragePoolTypeEnum.getPoolType(1));
        assertEquals(StoragePoolTypeEnum.ERASURE.getType(), StoragePoolTypeEnum.getPoolType(3));
    }

    @Test
    void testGetWrongCode() {
        assertEquals("", StoragePoolTypeEnum.getPoolType(-1));
    }
}
