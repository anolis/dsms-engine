/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagepool.request;

import com.alibaba.fastjson2.JSON;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import org.springframework.http.HttpMethod;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.Map;

public class AllowEcOverWrites extends RemoteRequest {
    public static final String ALLOW_EC_OVER_WRITES_SUCCESS = "set pool %s allow_ec_overwrites to true";

    public AllowEcOverWrites(String poolName) {
        if (ObjectUtils.isEmpty(poolName)) {
            throw new IllegalArgumentException("pool name must not be blank");
        }
        Map<String, Object> requestParam = new HashMap<>(4);
        requestParam.put("prefix", "osd pool set");
        requestParam.put("pool", poolName);
        requestParam.put("var", "allow_ec_overwrites");
        requestParam.put("val", "true");
        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}