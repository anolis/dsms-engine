package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.dsms.common.constant.AlertMessageAppriseStatusEnum;
import org.junit.jupiter.api.Test;

class AlertMessageAppriseStatusEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(AlertMessageAppriseStatusEnum.DOWN, AlertMessageAppriseStatusEnum.getByCode(0));
        assertEquals(AlertMessageAppriseStatusEnum.UP_BUT_NOT_APPRISED, AlertMessageAppriseStatusEnum.getByCode(1));
        assertEquals(AlertMessageAppriseStatusEnum.UP_AND_APPRISED, AlertMessageAppriseStatusEnum.getByCode(2));
    }

    @Test
    void testGetWrongCode() {
        assertNull(AlertMessageAppriseStatusEnum.getByCode(-1));
    }

    @Test
    void testGetters() {
        AlertMessageAppriseStatusEnum down = AlertMessageAppriseStatusEnum.DOWN;
        assertEquals(0, down.getCode());
        assertEquals("未开启", down.getStatus());
    }
}
