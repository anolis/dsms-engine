package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.TaskTypeEnum;
import com.dsms.common.constant.TaskTypeEnum.TypeConstants;
import org.junit.jupiter.api.Test;


class TaskTypeEnumTest {


    @Test
    void testGetters() {
        TaskTypeEnum addOsd = TaskTypeEnum.ADD_OSD;
        assertEquals(TypeConstants.ADD_OSD, addOsd.getType());
        assertEquals("节点管理磁盘", addOsd.getName());
    }

}
