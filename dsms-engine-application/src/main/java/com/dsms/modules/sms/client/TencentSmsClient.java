/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.sms.client;


import com.alibaba.fastjson2.JSON;
import com.dsms.common.constant.CommonEnum;
import com.dsms.modules.sms.AbstractSmsClient;
import com.dsms.modules.sms.config.SmsProviderProperties;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20210111.models.SendSmsResponse;
import com.tencentcloudapi.sms.v20210111.models.SendStatus;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/**
 * Tencent sms client
 */
@Slf4j
public class TencentSmsClient extends AbstractSmsClient {

    private SmsClient client;

    private static final String RESPONSE_OK = "Ok";


    public TencentSmsClient(SmsProviderProperties properties) {
        super(properties);
    }

    @Override
    protected void doInit() {
        Credential credential = new Credential(properties.getSmsAccessId(), properties.getSmsAccessSecret());
        ClientProfile clientProfile = new ClientProfile();
        if(properties.isProxyEnabled()){
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setProxyHost(properties.getProxyHost());
            httpProfile.setProxyPort(properties.getProxyPort());
            clientProfile.setHttpProfile(httpProfile);
        }
        client = new SmsClient(credential, properties.getSmsRegion(), clientProfile);
    }

    @Override
    protected void doSendSms(String uuid, String[] mobiles, String signName, String apiTemplateId, List<Map<String, Object>> templateParams) throws Throwable {
        SendSmsRequest request = new SendSmsRequest();
        request.setSmsSdkAppId(properties.getSmsAppId());
        request.setSignName(signName);
        request.setTemplateId(apiTemplateId);
        request.setTemplateParamSet(null);
        request.setPhoneNumberSet(mobiles);
        request.setSessionContext(uuid);
        SendSmsResponse smsResponse = null;
        try {
            smsResponse = this.client.SendSms(request);
            SendStatus sendStatus = smsResponse.getSendStatusSet()[0];
            if (!RESPONSE_OK.equals(sendStatus.getCode())) {
                throw new RuntimeException(sendStatus.getMessage());
            }
        } catch (Exception e) {
            log.error("Tencent sms send error. error message:{},request:{},response:{}", e.getMessage(), JSON.toJSONString(request), JSON.toJSONString(smsResponse), e);
            throw new RuntimeException(e.getMessage(),e);
        }
    }
}
