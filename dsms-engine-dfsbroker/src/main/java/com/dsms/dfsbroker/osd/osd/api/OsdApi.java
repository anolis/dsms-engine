/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.osd.osd.api;

import com.dsms.common.constant.CrushFailureDomainEnum;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.common.CommandDirector;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.dfsbroker.osd.osd.model.remote.OSDMetadataResult;
import com.dsms.dfsbroker.osd.osd.model.remote.OSDResult;
import com.dsms.dfsbroker.osd.osd.model.remote.OSDStatusResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OsdApi {
    public static final int MAX_RETRIES = CommonApi.DEFAULT_RETRY_TIMES;

    @Autowired
    private CommonApi commonApi;

    public OSDResult getOsdById(RemoteRequest remoteRequest, int osdId) throws Throwable {
        CommandDirector.constructGetOSDById(remoteRequest, osdId);
        return commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, OSDResult.class);
    }

    /**
     * move osd in crushmap
     *
     * @return RemoteResponse
     */
    public RemoteResponse updateOsdBucketRequest(RemoteRequest remoteRequest, int osdid, CrushFailureDomainEnum crushFailureDomainEnum, String targetBucket) throws Throwable {
        CommandDirector.constructUpdateOsdBucketRequest(remoteRequest, osdid, crushFailureDomainEnum, targetBucket);
        RemoteResponse remoteResponse = commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
        CommandDirector.constructGetRequest(remoteRequest, remoteResponse.getId());
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    public List<OSDStatusResult> listOsdStatus(RemoteRequest remoteRequest) throws Throwable {
        CommandDirector.constructListOSDStatus(remoteRequest);
        OSDStatusResult[] osdResults = commonApi.remoteCall(remoteRequest, MAX_RETRIES, OSDStatusResult[].class);
        return Arrays.stream(osdResults).collect(Collectors.toList());
    }

    /**
     * get osd metadata
     */

    public List<OSDMetadataResult> getOsdMetadata(RemoteRequest remoteRequest) throws Throwable{
        CommandDirector.constructOsdMetadataRequest(remoteRequest);
        OSDMetadataResult[] osdMetadataResults = commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, OSDMetadataResult[].class);
        return Arrays.stream(osdMetadataResults).collect(Collectors.toList());
    }

}
