/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.overview.controller;

import com.dsms.common.model.Result;
import com.dsms.modules.overview.model.dto.OverviewDTO;
import com.dsms.modules.overview.service.IOverviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/overview")
@Api(tags = "集群概览模块")
public class OverviewController {
    @Autowired
    IOverviewService overviewService;

    @ApiOperation("概览模块-获取集群概览")
    @PostMapping("/get")
    public Result<OverviewDTO> getOverView() {
        return overviewService.getOverView();
    }
}
