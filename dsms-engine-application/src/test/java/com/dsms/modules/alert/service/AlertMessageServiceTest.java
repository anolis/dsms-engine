/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.dsms.modules.alert.entity.AlertMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AlertMessageServiceTest {
    @Autowired
    private IAlertMessageService alertMessageService;


    @Test
    @DisplayName("alert-message crud")
    @Transactional
    void testNormal() {
        // mock a task bean
        AlertMessage alertMessage = new AlertMessage();
        alertMessage.setContent("content");
        alertMessage.setRuleId(1);
        // assert create feature
        boolean result = alertMessageService.save(alertMessage);
        assertTrue(result);
        AlertMessage alertMessageEntity = alertMessageService.getById(alertMessage.getId());
        assertNotNull(alertMessageEntity);

        // assert update feature
        alertMessageEntity.setContent("new content");
        boolean updateResult = alertMessageService.updateById(alertMessageEntity);
        Assertions.assertNotEquals(false, updateResult);
        assertEquals(alertMessage.getId(), alertMessageEntity.getId());
        AlertMessage alertMessageNew = alertMessageService.getById(alertMessageEntity.getId());
        assertEquals(alertMessageNew.getContent(), alertMessageEntity.getContent());

        // assert delete feature
        alertMessageService.removeById(alertMessageEntity.getId());
        AlertMessage alertAppriseToDelete = alertMessageService.getById(alertMessageEntity.getId());
        assertNull(alertAppriseToDelete);
    }
}