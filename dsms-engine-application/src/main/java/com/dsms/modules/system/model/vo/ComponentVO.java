/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.system.model.vo;

import com.dsms.dfsbroker.cluster.model.remote.ServerResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@ApiModel(value = "管理平台信息")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ComponentVO {
    @ApiModelProperty(value = "dsms engine version")
    private String dsmsEngineVersion;

    @ApiModelProperty(value = "dsms engine git commit id")
    private String dsmsEngineCommitId;

    @ApiModelProperty(value = "dsms engine git tag ids")
    private String dsmsEngineCommitTags;

    @ApiModelProperty(value = "dsms release version")
    private String dsmsReleaseVersion;

    @ApiModelProperty(value = "dsms storage ceph version")
    private List<StorageVersion> storageVersions;

    @NoArgsConstructor
    @Data
    @AllArgsConstructor
    public static class StorageVersion {
        @ApiModelProperty(value = "dsms storage node name")
        private String nodeName;
        @ApiModelProperty(value = "dsms storage node version")
        private String dsmsStorageVersion;

    }


    public static List<StorageVersion> parseServiceToStorageVersions(List<ServerResult> servers) {
        return servers.stream().map(server -> new StorageVersion(server.getHostname(), server.getCephVersion())).collect(Collectors.toList());
    }


}
