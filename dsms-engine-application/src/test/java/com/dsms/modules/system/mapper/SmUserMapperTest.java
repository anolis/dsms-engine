/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.system.mapper;

import com.dsms.modules.system.model.SmUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SmUserMapperTest {
    @Autowired
    private SmUserMapper smUserMapper;

    @Test
    @Transactional
    public void testNormal() throws Exception {
        // mock a user bean
        SmUser user = new SmUser();
        user.setUsername("test");
        // assert create feature
        int result = smUserMapper.insert(user);
        Assertions.assertNotEquals(result, 0);
        SmUser userEntity = smUserMapper.selectById(user.getId());
        Assertions.assertNotNull(userEntity);
        // assert update feature
        userEntity.setUsername("new_name");
        int result1 = smUserMapper.updateById(userEntity);
        Assertions.assertNotEquals(result1, 0);
        Assertions.assertEquals(user.getId(), userEntity.getId());
        SmUser userNew = smUserMapper.selectById(userEntity.getId());
        Assertions.assertEquals(userNew.getUsername(),userEntity.getUsername());
        // assert delete feature
        smUserMapper.deleteById(userEntity.getId());
        SmUser userToDelete = smUserMapper.selectById(userEntity.getId());
        Assertions.assertNull(userToDelete);
    }

}