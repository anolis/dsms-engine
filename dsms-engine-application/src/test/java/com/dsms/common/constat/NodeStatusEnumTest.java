package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.NodeStatusEnum;
import org.junit.jupiter.api.Test;


class NodeStatusEnumTest {


    @Test
    void testGetters() {
        NodeStatusEnum down = NodeStatusEnum.DOWN;
        assertEquals(0, down.getStatus());
        assertEquals("离线", down.getMessage());
    }

}
