package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.AlertRuleLevelEnum;
import com.dsms.common.constant.AlertRuleTypeEnum;
import org.junit.jupiter.api.Test;

class AlertRuleTypeEnumTest {

    @Test
    void testGetByCode() {
        assertEquals(AlertRuleTypeEnum.CPU_USAGE, AlertRuleTypeEnum.getalertRuleTypeEnum("cpu.usage"));
    }

    @Test
    void testGetWrongCode() {
        assertEquals(AlertRuleTypeEnum.UNKNOW, AlertRuleTypeEnum.getalertRuleTypeEnum("test"));
    }

    @Test
    void testGetters() {
        AlertRuleTypeEnum cpuUsage = AlertRuleTypeEnum.CPU_USAGE;
        assertEquals("cpu使用率(%)", cpuUsage.getName());
        assertEquals("cpu.usage", cpuUsage.getMetric());
    }
}
