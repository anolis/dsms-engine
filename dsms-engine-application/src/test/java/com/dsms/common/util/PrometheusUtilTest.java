package com.dsms.common.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.PromQL;
import org.junit.jupiter.api.Test;

class PrometheusUtilTest {

    @Test
    public void testPrometheusUtil() {
        String exceptResult = "rate(node_network_receive_drop_total{instance=\"ceph1\"}[1m])";
        assertEquals(exceptResult, PrometheusUtils.getFormatPromQL(PromQL.NODE_NETWORK_RECEIVE_DROP_RATE, new String[]{"ceph1"}));
    }

}