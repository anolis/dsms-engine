/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * Task status enum
 */
@Getter
@AllArgsConstructor
public enum TaskStatusEnum {
    /**
     * add task not start
     */
    INIT(0, "未开始"),
    /**
     * task waiting executor execute
     */
    QUEUE(1, "队列中"),
    /**
     * executor running task
     */
    EXECUTING(2, "执行中"),
    /**
     * task run finished
     */
    FINISH(3, "完成"),
    /**
     * task run failed
     */
    FAIL(4, "失败"),
    /**
     * task run failed,and turn to rollback
     */
    ROLLBACK(5, "任务失败,回滚中"),
    /**
     * task run failed,but rollback success
     */
    ROLLBACK_SUCCESS(6, "任务失败,回滚成功");

    private final int status;
    private final String message;

}
