/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;

public class FilterUtil {

    private FilterUtil() {
    }

    public static boolean matches(Object exceptParam, Object actualParam) {
        if (exceptParam != null) {
            return ObjectUtil.equals(exceptParam, actualParam);
        }
        return true;
    }

    public static boolean fuzzy(String exceptParam, String actualParam) {
        if (StrUtil.isNotBlank(exceptParam)) {
            return actualParam.contains(exceptParam);
        }
        return true;
    }

}
