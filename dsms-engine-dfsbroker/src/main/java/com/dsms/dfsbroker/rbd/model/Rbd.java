/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.rbd.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "存储卷对象", description = "存储卷信息表")
public class Rbd {
    //Each rbd needs to reserve 100M metadata space
    public static final long RBD_META_DATA_BYTES = 100L * 1024 * 1024;

    @ApiModelProperty("存储卷id")
    private int id;
    @ApiModelProperty("存储卷名称")
    private String rbdName;
    @ApiModelProperty("存储池名称")
    private String poolName;
    @ApiModelProperty("数据池名称")
    private String dataPoolName;
    @ApiModelProperty("存储卷已使用容量")
    private Long usedSize;
    @ApiModelProperty("存储卷总容量")
    private Long totalSize;
}
