/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagepool.api;

import com.dsms.ClusterProperties;
import com.dsms.common.constant.RemoteResponseStatusEnum;
import com.dsms.common.remotecall.model.FinishedDetail;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.dfsbroker.storagepool.model.dto.ErasureCreateDTO;
import com.dsms.dfsbroker.storagepool.model.dto.ReplicatedCreateDTO;
import com.dsms.dfsbroker.storagepool.request.CreateStoragePoolRequest;
import com.dsms.modules.util.RemoteCallUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import java.time.Duration;
import java.util.List;

import static org.apache.commons.lang3.ThreadUtils.sleep;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@EnabledIf(expression = "${dsms.storage.enable}", loadContext = true)
class StoragePoolApiTest {
    private static RemoteRequest request;
    @Autowired
    StoragePoolApi storagePoolApi;
    @Autowired
    private ClusterProperties properties;

    @BeforeEach
    public void setUp() {
        Cluster cluster = properties.getTestCluster();
        RemoteCallUtil.updateRemoteFixedParam(cluster);
        request = RemoteCallUtil.generateRemoteRequest();
    }

    @Test
    void getPoolByPooId() {
        Assertions.assertThatCode(() -> storagePoolApi.getStoragePoolByPoolId(request, 2)).doesNotThrowAnyException();
    }

    @Test
    void getPoolList() {
        Assertions.assertThatCode(() -> storagePoolApi.getStoragePoolList(request)).doesNotThrowAnyException();
    }

    @Test
    void getPoolDf() {
        Assertions.assertThatCode(() -> storagePoolApi.getDf(request)).doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void createPoolNormal() {
        ErasureCreateDTO erasureCreateDTO = new ErasureCreateDTO("apiTest_root", 3, 32, 32, null);
        RemoteResponse response = storagePoolApi.createPool(request, erasureCreateDTO);
        sleep(Duration.ofMillis(500));
        RemoteResponse createPoolResult = storagePoolApi.getCreatePoolResult(request, response.getId());
        assertEquals(RemoteResponseStatusEnum.SUCCESS.getMessage(), createPoolResult.getState());
        List<FinishedDetail> finished = createPoolResult.getFinished();
        String outs = finished.get(0).getOuts();
        assertEquals(outs, String.format(CreateStoragePoolRequest.Create_POOL_SUCCESS, erasureCreateDTO.getPoolName()));
    }

    @Test
    @SneakyThrows
    void createPoolException() {
        ReplicatedCreateDTO replicatedCreateDTO = new ReplicatedCreateDTO("apiTest_err", 1, 0, 0, 0, "not_exist_rule");
        RemoteResponse response = storagePoolApi.createPool(request, replicatedCreateDTO);
        assertNotNull(response.getId());
        sleep(Duration.ofMillis(200));
        assertTrue(storagePoolApi.getCreatePoolResult(request, response.getId()).isHasFailed());
    }

    @Test
    void deletePool() {
        Assertions.assertThatCode(() -> storagePoolApi.deletePool(request, "testPool")).doesNotThrowAnyException();
    }

    @Test
    void allowEcOverWrites() {
        Assertions.assertThatCode(() -> storagePoolApi.allowEcOverWrites(request,"testPool")).doesNotThrowAnyException();
    }
}
