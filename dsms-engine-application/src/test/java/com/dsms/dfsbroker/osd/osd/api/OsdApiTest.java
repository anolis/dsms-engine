/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.osd.osd.api;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;

import com.dsms.ClusterProperties;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import com.dsms.common.constant.CrushFailureDomainEnum;
import com.dsms.common.constant.OdsStatusEnum;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.dfsbroker.osd.osd.model.remote.OSDMetadataResult;
import com.dsms.dfsbroker.osd.osd.model.remote.OSDStatusResult;
import com.dsms.modules.util.RemoteCallUtil;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.EnabledIf;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@EnabledIf(expression = "${dsms.storage.enable}", loadContext = true)
class OsdApiTest {
    private static RemoteRequest request;
    @Autowired
    private OsdApi osdApi;
    @Autowired
    private ClusterProperties properties;

    @BeforeEach
    public void setUp() {
        Cluster cluster = properties.getTestCluster();
        RemoteCallUtil.updateRemoteFixedParam(cluster);
        request = RemoteCallUtil.generateRemoteRequest();
    }

    @Test
    void getOsdById() {
        Assertions.assertThatCode(() -> osdApi.getOsdById(request, 0)).doesNotThrowAnyException();
    }

    @Test
    void updateOsdBucket() {
        Assertions.assertThatCode(() -> osdApi.updateOsdBucketRequest(request, 0, CrushFailureDomainEnum.HOST, "node1")).doesNotThrowAnyException();
    }

    @Test
    @SneakyThrows
    void listOsdStatus() {
        List<OSDStatusResult> osdStatusResults = osdApi.listOsdStatus(request);
        List<String> osdStatusEnum = Arrays.stream(OdsStatusEnum.values()).map(OdsStatusEnum::getStatus).collect(Collectors.toList());
        assertNotNull(osdStatusResults,"osd status results is empty");
        for (OSDStatusResult osdStatusResult : osdStatusResults) {
            assertThat(osdStatusResult.getState(), everyItem(is(in(osdStatusEnum))));
        }
    }

    @Test
    @SneakyThrows
    void listOsdMetadate() {
        List<OSDMetadataResult> osdMetadata = osdApi.getOsdMetadata(request);
        assertNotNull(osdMetadata,"osd metadata results is empty");
    }

}