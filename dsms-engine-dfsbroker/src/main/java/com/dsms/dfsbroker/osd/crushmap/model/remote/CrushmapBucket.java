/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.osd.crushmap.model.remote;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class CrushmapBucket {


    @JsonProperty("buckets")
    private List<BucketsDTO> buckets;

    /*
        the bucket in crushmap
     */
    @NoArgsConstructor
    @Data
    public static class BucketsDTO {
        @JsonProperty("id")
        private Integer id;
        @JsonProperty("name")
        private String name;
        //the crush failure domain id of bucket
        @JsonProperty("type_id")
        private Integer typeId;
        //the bucket type name,only support "HOST" and "ROOT" now
        @JsonProperty("type_name")
        private String typeName;
        //the weight of bucket,default number is 1.0
        @JsonProperty("weight")
        private Integer weight;
        //the algorithm of crush: default: straw2
        @JsonProperty("alg")
        private String alg;
        //the method to hash, default: rjenkins1
        @JsonProperty("hash")
        private String hash;
        //the sub item managed by this bucket
        //it's osd when the bucket type is HOST,while HOST when is ROOT
        @JsonProperty("items")
        private List<ItemsDTO> items;

        @NoArgsConstructor
        @Data
        public static class ItemsDTO {
            @JsonProperty("id")
            private Integer id;
            @JsonProperty("weight")
            private Integer weight;
            //the index of itemDTO in items
            @JsonProperty("pos")
            private Integer pos;
        }
    }
}
