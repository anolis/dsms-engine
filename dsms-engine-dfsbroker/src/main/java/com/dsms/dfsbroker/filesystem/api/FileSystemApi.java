/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.filesystem.api;

import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.common.CommandDirector;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.dfsbroker.filesystem.model.dto.FileSystemDTO;
import com.dsms.dfsbroker.filesystem.model.remote.FsLsResponse;
import com.dsms.dfsbroker.filesystem.model.remote.FsStatusResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FileSystemApi {
    public static final int MAX_RETRIES = CommonApi.DEFAULT_RETRY_TIMES;

    @Autowired
    private CommonApi commonApi;

    /**
     * obtain the file system information of the storage pool
     *
     * @return FsResponse
     */
    public List<FsLsResponse> getFsLs(RemoteRequest remoteRequest) throws Throwable {
        CommandDirector.constructFsLsRequest(remoteRequest);
        FsLsResponse[] fsLsResponses = commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, FsLsResponse[].class);
        return Arrays.stream(fsLsResponses).collect(Collectors.toList());
    }

    /**
     * obtain the file system status
     *
     * @return FsStatusResponse
     */
    public FsStatusResponse getFsStatus(RemoteRequest remoteRequest) throws Throwable {
        CommandDirector.constructFsStatusRequest(remoteRequest);
        return commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, FsStatusResponse.class);
    }

    /**
     * make new filesystem using named pools metadata and data
     */
    public RemoteResponse createFs(RemoteRequest remoteRequest, FileSystemDTO fileSystem) throws Throwable {
        CommandDirector.constructCreateFsRequest(remoteRequest, fileSystem);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * remove the named filesystem
     */
    public RemoteResponse removeFs(RemoteRequest remoteRequest, FileSystemDTO fileSystem) throws Throwable {
        CommandDirector.constructRemoveFsRequest(remoteRequest, fileSystem);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * fail the named filesystem
     */
    public RemoteResponse failFs(RemoteRequest remoteRequest, FileSystemDTO fileSystem) throws Throwable {
        CommandDirector.constructFailFsRequest(remoteRequest, fileSystem);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }
}
