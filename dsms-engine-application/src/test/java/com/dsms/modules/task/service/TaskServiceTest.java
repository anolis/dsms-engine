/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.task.service;

import com.dsms.common.constant.TaskStatusEnum;
import com.dsms.common.constant.TaskTypeEnum;
import com.dsms.common.taskmanager.TaskContext;
import com.dsms.common.taskmanager.model.AsyncTask;
import com.dsms.common.taskmanager.model.Task;
import com.dsms.common.taskmanager.service.ITaskService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TaskServiceTest {
    @Autowired
    private ITaskService taskService;

    @Autowired
    private TaskContext taskContext;


    @Test
    @DisplayName("任务模块crud")
    @Transactional
    void testNormal() {
        // mock a task bean
        Task task = new Task();
        task.setTaskName("test");
        task.setTaskMessage("任务描述");
        task.setTaskParam("1231412");
        task.setTaskStatus(1);
        task.setTaskEndTime(LocalDateTime.MIN);
        // assert create feature
        boolean result = taskService.save(task);
        assertTrue(result);
        Task taskEntity = taskService.getById(task.getId());
        assertNotNull(taskEntity);
        // assert update feature
        taskEntity.setTaskName("new_name");
        boolean result1 = taskService.updateById(taskEntity);
        assertTrue(result1);
        assertEquals(task.getId(), taskEntity.getId());
        Task taskNew = taskService.getById(taskEntity.getId());
        assertEquals(taskNew.getTaskName(), taskEntity.getTaskName());
        // assert delete feature
        taskService.removeById(taskEntity.getId());
        Task taskToDelete = taskService.getById(taskEntity.getId());
        assertNull(taskToDelete);
    }

    @Test
    @Transactional
    void testOsdRemoveTaskExist() {
        Integer osdId = 0;
        // mock a remove osd task bean
        Task task = new AsyncTask();
        task.setTaskName(TaskTypeEnum.REMOVE_OSD.getName());
        task.setTaskType(TaskTypeEnum.REMOVE_OSD.getType());
        task.setTaskParam(String.valueOf(osdId));
        task.setTaskMessage("");
        task.setTaskStatus(TaskStatusEnum.QUEUE.getStatus());
        taskService.save(task);
        boolean b = taskContext.validateTask(TaskTypeEnum.REMOVE_OSD, String.valueOf(osdId));
        Assertions.assertFalse(b);

    }

    @Test
    @Transactional
    void testOsdAddTaskExist() {
        String nodeName = "node1";
        String devicePath = "/dev/sda";
        // mock a add osd task bean
        Task task = new AsyncTask();
        task.setTaskName(TaskTypeEnum.ADD_OSD.getName());
        task.setTaskType(TaskTypeEnum.ADD_OSD.getType());
        task.setTaskParam("");
        task.setTaskMessage(nodeName + devicePath);
        task.setTaskStatus(TaskStatusEnum.QUEUE.getStatus());
        taskService.save(task);
        boolean validateTask = taskContext.validateTask(TaskTypeEnum.ADD_OSD, nodeName, devicePath);
        Assertions.assertFalse(validateTask);

    }

}