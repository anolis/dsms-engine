package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.TaskStatusEnum;
import org.junit.jupiter.api.Test;


class TaskStatusEnumTest {


    @Test
    void testGetters() {
        TaskStatusEnum init = TaskStatusEnum.INIT;
        assertEquals(0, init.getStatus());
        assertEquals("未开始", init.getMessage());
    }

}
