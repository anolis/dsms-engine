/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.constant;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AlertRuleTypeEnum {
    UNKOWN("unkown", "未知参数"),
    CPU_USAGE("cpu.usage", "cpu使用率(%)"),
    MEMORY_USAGE("memory.usage", "内存使用率(%)"),
    DEVICE_USAGE("device.usage", "磁盘使用率(%)"),
    CEPH_HEALTH_WARN("ceph.health.warn", "集群状态告警"),
    CEPH_HEALTH_ERROR("ceph.health.error", "集群状态异常"),
    NETWORK_DROP("network.drop", "网卡丢包率"),
    NETWORK_ERROR("network.error", "网卡错误率"),
    OSD_SLOW("osd.slow", "OSDs响应速率"),
    MON_COUNT("mon.count", "mon节点数"),
    OSD_DOWN("osd.down", "OSDs停用率(%)"),
    POOL_CAPACITY("pool.capacity", "存储池使用率(%)"),
    PG_INACTIVE("pg.inactive", "pgs不活跃数");

    private final String metric;
    private final String name;


    public static AlertRuleTypeEnum getalertRuleTypeEnum(String metric){
        for(AlertRuleTypeEnum alertRuleTypeEnum: AlertRuleTypeEnum.values()){
            if(alertRuleTypeEnum.getMetric().equals(metric)){
                return alertRuleTypeEnum;
            }
        }
        return UNKOWN;
    }
}
