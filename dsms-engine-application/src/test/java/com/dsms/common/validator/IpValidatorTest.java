package com.dsms.common.validator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class IpValidatorTest {

    @Test
    void testIsValid() {
        IpValidator ipValidator = new IpValidator();
        Assertions.assertTrue(ipValidator.isValid("127.0.0.1", null));
        Assertions.assertFalse(ipValidator.isValid("256.100.100.100", null));
        Assertions.assertTrue(ipValidator.isValid("0:0:0:0:0:0:0:0", null));
        Assertions.assertFalse(ipValidator.isValid("0:0:0:0:0:0:0:0:z", null));
    }
}