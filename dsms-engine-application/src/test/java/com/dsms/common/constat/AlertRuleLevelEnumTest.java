package com.dsms.common.constat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.dsms.common.constant.AlertMessageStatusEnum;
import com.dsms.common.constant.AlertRuleLevelEnum;
import org.junit.jupiter.api.Test;

class AlertRuleLevelEnumTest {

    @Test
    void testGetters() {
        AlertRuleLevelEnum hint = AlertRuleLevelEnum.HINT;
        assertEquals(0, hint.getStatus().intValue());
        assertEquals("提示", hint.getMessage());

        AlertRuleLevelEnum warning = AlertRuleLevelEnum.WARNING;
        assertEquals(1, warning.getStatus().intValue());
        assertEquals("一般", warning.getMessage());

        AlertRuleLevelEnum error = AlertRuleLevelEnum.ERROR;
        assertEquals(2, error.getStatus().intValue());
        assertEquals("重要", error.getMessage());

        AlertRuleLevelEnum critical = AlertRuleLevelEnum.CRITICAL;
        assertEquals(3, critical.getStatus().intValue());
        assertEquals("紧急", critical.getMessage());
    }

}
