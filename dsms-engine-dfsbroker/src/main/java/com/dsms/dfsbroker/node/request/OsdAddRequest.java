/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.node.request;

import com.alibaba.fastjson2.JSON;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import org.springframework.http.HttpMethod;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

public class OsdAddRequest extends RemoteRequest {

    public static final String ADD_OSD_FAIL = "Created no osd(s)";
    public static final String ADD_OSD_SUCCESS = "Created osd(s)";
    public static final String ADD_OSD_SUCCESS_PATTERN = "Created osd\\(s\\) (\\d+) on host '(.+)'";


    public OsdAddRequest(String nodeName, String devicePath) {
        if (ObjectUtils.isEmpty(nodeName) || ObjectUtils.isEmpty(devicePath)) {
            throw new IllegalArgumentException("nodeName and devicePath must not be blank");
        }
        StringJoiner stringJoiner = new StringJoiner(":");
        stringJoiner.add(nodeName);
        stringJoiner.add(devicePath);
        Map<String, Object> requestParam = new HashMap<>(4);
        requestParam.put("prefix", "orch daemon add osd");
        requestParam.put("svc_arg", stringJoiner.toString());
        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}
