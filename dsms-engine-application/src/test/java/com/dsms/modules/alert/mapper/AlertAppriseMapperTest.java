/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.mapper;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.dsms.modules.alert.entity.AlertApprise;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AlertAppriseMapperTest {
    @Autowired
    private AlertAppriseMapper alertAppriseMapper;


    @Test
    @DisplayName("alert-apprise mapper crud")
    @Transactional
    void testNormal() {
        // mock a task bean
        AlertApprise alertApprise = new AlertApprise();
        alertApprise.setSmtpHost("xxx.mail.com");
        alertApprise.setSmtpPort(25);
        // assert create feature
        int result = alertAppriseMapper.insert(alertApprise);
        Assertions.assertNotEquals(0, result);
        AlertApprise alertAppriseEntity = alertAppriseMapper.selectById(alertApprise.getId());
        assertNotNull(alertAppriseEntity);

        // assert update feature
        alertAppriseEntity.setSmtpHost("new.mail.com");
        int result1 = alertAppriseMapper.updateById(alertAppriseEntity);
        Assertions.assertNotEquals(0, result1);
        assertEquals(alertApprise.getId(), alertAppriseEntity.getId());
        AlertApprise alertAppriseNew = alertAppriseMapper.selectById(alertAppriseEntity.getId());
        assertEquals(alertAppriseNew.getSmtpHost(), alertAppriseEntity.getSmtpHost());

        // assert delete feature
        alertAppriseMapper.deleteById(alertAppriseEntity.getId());
        AlertApprise alertAppriseToDelete = alertAppriseMapper.selectById(alertAppriseEntity.getId());
        assertNull(alertAppriseToDelete);
    }
}