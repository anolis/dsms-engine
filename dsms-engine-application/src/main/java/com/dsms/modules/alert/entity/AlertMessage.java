/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("alert_message")
@Accessors(chain = true)
@ApiModel(value = "AlertMessage对象", description = "告警信息表")
public class AlertMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("告警信息id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("告警规则id")
    private Integer ruleId;

    @ApiModelProperty("告警信息内容")
    private String content;

    @ApiModelProperty("告警规则模块")
    private String module;

    @ApiModelProperty("告警级别（0-提示、1-一般、2-重要、3-紧急）")
    private Integer level;

    @ApiModelProperty("告警产生时间")
    private LocalDateTime createTime;

    @ApiModelProperty("告警确认时间")
    private LocalDateTime updateTime;

    @ApiModelProperty("告警信息状态（0-未读，1-已读）")
    private Integer status;

    @ApiModelProperty("告警次数")
    private Integer times;

    @ApiModelProperty("短信通知状态（0-未开启，1-开启但未通知，2-开启且已通知）")
    private Integer smsStatus;

    @ApiModelProperty("邮件通知状态（0-未开启，1-开启但未通知，2-开启且已通知）")
    private Integer emailStatus;
}
