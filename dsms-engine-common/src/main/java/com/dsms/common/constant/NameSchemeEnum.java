/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * regular expressions and validation failure messages for resource naming conventions
 */
@Getter
@AllArgsConstructor
public enum NameSchemeEnum {
    
    //common validate
    COMMON(ResourceConst.RESOURCE, RegexConstants.COMMON_REGEX, MessageConstants.COMMON_MESSAGE),
    STORAGE_VOLUME(ResourceConst.STORAGE_VOLUME, RegexConstants.COMMON_REGEX, MessageConstants.COMMON_MESSAGE),
    FILESYSTEM(ResourceConst.FILESYSTEM, RegexConstants.COMMON_REGEX, MessageConstants.COMMON_MESSAGE),
    STORAGE_DIR(ResourceConst.STORAGE_DIR, RegexConstants.COMMON_REGEX, MessageConstants.COMMON_MESSAGE),

    //special validate
    STORAGE_POOL(ResourceConst.STORAGE_POOL, RegexConstants.STORAGE_POOL_REGEX, MessageConstants.STORAGE_POOL_MESSAGE),
    CLUSTER(ResourceConst.CLUSTER, RegexConstants.CLUSTER_REGEX, MessageConstants.CLUSTER_MESSAGE);


    private final String resourceName;
    private final String regex;
    private final String message;


    public static final class MessageConstants {
        public static final String COMMON_MESSAGE = "名称格式不合法，最长16个字符，只能包含字母、数字和下划线";
        public static final String STORAGE_POOL_MESSAGE = "名称格式不合法，最长16个字符，只能包含字母和数字";
        public static final String CLUSTER_MESSAGE = "名称格式不合法，最长32个字符，只能包含字母、数字和下划线";

        private MessageConstants() {
        }
    }

    public static final class RegexConstants {
        public static final String COMMON_REGEX = "^\\w{1,16}$";
        public static final String STORAGE_POOL_REGEX = "^[a-zA-Z0-9]{1,16}$";
        public static final String CLUSTER_REGEX = "^[a-zA-Z0-9_]{1,32}$";

        private RegexConstants() {
        }
    }
}
