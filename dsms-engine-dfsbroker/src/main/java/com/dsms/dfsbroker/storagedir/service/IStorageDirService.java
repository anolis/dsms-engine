/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagedir.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.model.PageParam;
import com.dsms.dfsbroker.storagedir.model.StorageDir;
import com.dsms.dfsbroker.storagedir.model.dto.StorageDirDTO;

import javax.validation.Valid;
import java.util.List;

public interface IStorageDirService {
    /**
     * get dsms-storage storage dir info
     *
     * @return storage dir info list
     */
    List<StorageDir> list();


    /**
     * get dsms-storage storage dir info with page param
     *
     * @return nodes info list
     */
    Page<StorageDir> page(PageParam pageParam);

    /**
     * create dsms-storage storage dir
     *
     * @return result
     */
    boolean createStorageDir(@Valid StorageDirDTO storageDirDTO);

    /**
     * delete dsms-storage storage dir
     *
     * @return result
     */
    boolean deleteStorageDir(@Valid StorageDirDTO storageDirDTO);


}
