/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.dsms.modules.alert.entity.AlertRule;
import com.dsms.modules.alert.model.vo.AlertRulePageVO;
import com.dsms.modules.alert.model.vo.AlertRuleQueryVO;
import com.dsms.modules.alert.model.vo.AlertRuleUpdateVO;

/**
 * AlertRule operation service
 */
public interface IAlertRuleService extends IService<AlertRule> {
    /**
     * Get the alert rule page
     * @param alertRulePageVO
     * @return
     */
    Page<AlertRule> getAlertRulePage(AlertRulePageVO alertRulePageVO);

    /**
     * Update the alert rule config
     * @param alertRuleUpdateVO
     * @return
     */
    Boolean updateAlertRule(AlertRuleUpdateVO alertRuleUpdateVO);


    /**
     * Reset the alert rule config
     * @param alertRuleId
     * @return
     */
    Boolean resetAlertRule(AlertRuleQueryVO alertRuleId);


}
