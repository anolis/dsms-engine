/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.prometheus.converter.result;

import com.dsms.common.prometheus.converter.Data;

import java.util.HashMap;
import java.util.Map;

@lombok.Data
public class MatrixData implements Data {
    private Map<String, String> metric = new HashMap<>();
    private QueryResultItemValue[] dataValues;

    /**
     * get the two-dimensional array of dataValues
     *
     * @return two-dimensional
     */
    public double[][] getRangeQueryResult() {
        double[][] resultItem = new double[dataValues.length][2];
        for (int i = 0; i < dataValues.length; i++) {
            resultItem[i][0] = dataValues[i].getTimestamp();
            resultItem[i][1] = dataValues[i].getValue();
        }
        return resultItem;
    }
}
