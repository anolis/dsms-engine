/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.util;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import java.lang.reflect.Field;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ObjectUtil {

    private ObjectUtil() {
    }

    /**
     * check if the object has null or blank field
     *
     * @return boolean
     */
    public static boolean checkObjFieldContainNull(Object obj) {

        if (null == obj) {
            return true;
        }

        boolean flag = false;

        Class<?> clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            Object value;
            try {
                value = field.get(obj);
                // not allow any null field
                if (value == null || StringUtils.isBlank(value.toString())) {
                    flag = true;
                    break;
                }
            } catch (IllegalAccessException e) {
                log.error("check obj field failed", e);
            }
        }

        return flag;
    }

}
