/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.sms.model.convert;

import com.dsms.modules.alert.entity.AlertApprise;
import com.dsms.modules.sms.model.SmsSendMessage;
import java.util.List;

public class SmsMessageConvert {

    private SmsMessageConvert() {
    }

    public static SmsSendMessage convert(AlertApprise alertApprise, List<String> mobiles) {
        if (alertApprise == null) {
            return null;
        }

        SmsSendMessage smsSendMessage = new SmsSendMessage();

        smsSendMessage.setMobiles(mobiles.toArray(new String[0]));
        smsSendMessage.setSmsProvider(alertApprise.getSmsProvider());
        smsSendMessage.setSmsSignName(alertApprise.getSmsSignName());
        smsSendMessage.setSmsTemplate(alertApprise.getSmsTemplateCode());

        return smsSendMessage;
    }

}
