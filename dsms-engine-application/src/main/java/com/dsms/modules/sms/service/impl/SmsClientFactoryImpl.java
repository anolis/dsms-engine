/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.sms.service.impl;


import com.dsms.common.constant.AlertSmsProviderEnum;
import com.dsms.common.constant.ResultCode;
import com.dsms.common.exception.DsmsEngineException;
import com.dsms.modules.alert.entity.AlertApprise;
import com.dsms.modules.sms.AbstractSmsClient;
import com.dsms.modules.sms.SmsClient;
import com.dsms.modules.sms.SmsClientFactory;
import com.dsms.modules.sms.client.AliyunSmsClient;
import com.dsms.modules.sms.client.TencentSmsClient;
import com.dsms.modules.sms.config.SmsProviderProperties;
import com.dsms.modules.sms.model.convert.SmsProviderPropertiesConvert;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class SmsClientFactoryImpl implements SmsClientFactory {

    @Value("${sms.proxy.enabled:false}")
    private boolean proxyEnabled;

    @Value("${sms.proxy.Scheme:http}")
    private String proxyScheme;

    @Value("${sms.proxy.host:127.0.0.1}")
    private String proxyHost;

    @Value("${sms.proxy.port:0}")
    private Integer proxyPort;

    /**
     * Sms client map
     */
    private final ConcurrentMap<String, AbstractSmsClient> smsClients = new ConcurrentHashMap<>();

    @Override
    public SmsClient getSmsClient(String smsProvider) {
        return smsClients.get(smsProvider);
    }

    @Override
    public void createOrUpdateSmsClient(AlertApprise alertApprise){
        SmsProviderProperties properties = SmsProviderPropertiesConvert.convert(alertApprise);
        AbstractSmsClient client = smsClients.get(properties.getSmsProvider());
        if (client == null) {
            client = this.createSmsClient(properties);
            client.init();
            smsClients.put(client.getSmsProvider(), client);
        } else {
            client.refresh(properties);
        }
    }

    private AbstractSmsClient createSmsClient(SmsProviderProperties properties) {
        AlertSmsProviderEnum channelEnum = AlertSmsProviderEnum.getByCode(properties.getSmsProvider());
        properties.setProxyEnabled(proxyEnabled);
        properties.setProxyScheme(proxyScheme);
        properties.setProxyHost(proxyHost);
        properties.setProxyPort(proxyPort);
        switch (channelEnum) {
            case ALIYUN:
                return new AliyunSmsClient(properties);
            case TENCENT:
                return new TencentSmsClient(properties);
            default:
                throw new DsmsEngineException(ResultCode.SMS_PROVIDER_EMPTY);
        }
    }

}
