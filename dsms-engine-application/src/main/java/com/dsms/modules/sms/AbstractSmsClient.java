/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.sms;

import com.dsms.modules.sms.config.SmsProviderProperties;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/**
 * Sms client abstract class
 */
@Slf4j
public abstract class AbstractSmsClient implements SmsClient {

    /**
     * Sms client properties
     */
    protected SmsProviderProperties properties;


    public AbstractSmsClient(SmsProviderProperties properties) {
        this.properties = properties;
    }

    /**
     * Custom initialization
     */
    public final void init() {
        doInit();
    }

    /**
     * Custom initialization
     */
    protected abstract void doInit();

    public final void refresh(SmsProviderProperties properties) {
        if (properties.equals(this.properties)) {
            return;
        }
        this.properties = properties;
        this.init();
    }

    @Override
    public String getSmsProvider() {
        return properties.getSmsProvider();
    }

    @Override
    public final void sendSms(String uuid, String[] mobiles, String signName, String apiTemplateId, List<Map<String, Object>> templateParams) {
        try {
            doSendSms(uuid, mobiles, signName, apiTemplateId, templateParams);
        } catch (Throwable ex) {
            log.error("Send sms error,error message:{}", ex.getMessage(), ex);
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    protected abstract void doSendSms(String uuid, String[] mobiles, String signName, String apiTemplateId, List<Map<String, Object>> templateParams) throws Throwable;

}
