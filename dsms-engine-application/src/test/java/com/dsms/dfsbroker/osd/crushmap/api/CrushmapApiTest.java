/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.osd.crushmap.api;

import com.dsms.ClusterProperties;
import com.dsms.common.constant.RemoteResponseStatusEnum;
import com.dsms.common.remotecall.model.FinishedDetail;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.modules.util.RemoteCallUtil;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import java.time.Duration;
import java.util.List;

import static org.apache.commons.lang3.ThreadUtils.sleep;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@EnabledIf(expression = "${dsms.storage.enable}", loadContext = true)
public class CrushmapApiTest {
    private static RemoteRequest request;
    @Autowired
    CrushmapApi crushmapApi;
    @Autowired
    private ClusterProperties properties;

    private static final String BUCKET_NAME = "apiTest_bucket";

    @BeforeEach
    public void setUp() {
        Cluster cluster = properties.getTestCluster();
        RemoteCallUtil.updateRemoteFixedParam(cluster);
        request = RemoteCallUtil.generateRemoteRequest();
    }

    @Test
    void createBucket() throws Throwable {
        RemoteResponse response = crushmapApi.createBucket(request, BUCKET_NAME, 1, "");
        RemoteResponse createEcProfileResult = crushmapApi.getUpdateCrushmapResult(RemoteCallUtil.generateRemoteRequest(), response.getId());
        sleep(Duration.ofMillis(500));
        assertEquals(RemoteResponseStatusEnum.SUCCESS.getMessage(), createEcProfileResult.getState());
        List<FinishedDetail> finished = createEcProfileResult.getFinished();
        String outs = finished.get(0).getOuts();
        assertNull(outs);
    }

    @Test
    void createRule() throws Throwable {
        RemoteResponse response = crushmapApi.createRule(request, BUCKET_NAME);
        RemoteResponse createEcProfileResult = crushmapApi.getUpdateCrushmapResult(RemoteCallUtil.generateRemoteRequest(), response.getId());
        sleep(Duration.ofMillis(500));
        assertEquals(RemoteResponseStatusEnum.SUCCESS.getMessage(), createEcProfileResult.getState());
        List<FinishedDetail> finished = createEcProfileResult.getFinished();
        String outs = finished.get(0).getOuts();
        assertNull(outs);
    }

    @Test
    void getCrushmapBuckets() {
        Assertions.assertThatCode(() -> crushmapApi.getCrushmapBuckets(request)).doesNotThrowAnyException();
    }

    @Test
    void delBucket() {
        Assertions.assertThatCode(() -> crushmapApi.delBucket(request, BUCKET_NAME)).doesNotThrowAnyException();
    }

    @Test
    void delCrushRule() {
        Assertions.assertThatCode(() -> crushmapApi.delCrushRule(request, BUCKET_NAME)).doesNotThrowAnyException();
    }

}
