package com.dsms.common.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.taskmanager.model.Task;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;

class PageUtilTest {

    @Test
    void testPageUtil() {
        List<Task> tasks = new ArrayList<>();
        int pageSize = 10;

        for (int i = 0; i < pageSize + 1; i++) {
            tasks.add(new Task());
        }
        List<Task> pagedTask = PageUtil.pageData(tasks, 1, pageSize);
        assertEquals(pageSize, pagedTask.size());
    }

    @Test
    void testPageUtilEmpty() {
        List<Task> tasks = new ArrayList<>();
        int pageSize = 10;

        for (int i = 0; i < pageSize + 1; i++) {
            tasks.add(new Task());
        }
        List<Task> pagedTask = PageUtil.pageData(tasks, 3, pageSize);
        assertEquals(Collections.emptyList(), pagedTask);
    }

    @Test
    void testGetPageData() {
        List<Task> tasks = new ArrayList<>();
        int pageSize = 10;

        for (int i = 0; i < pageSize + 1; i++) {
            tasks.add(new Task());
        }
        Page<Task> pageData = PageUtil.getPageData(tasks, 1, pageSize);
        assertEquals(pageSize + 1, pageData.getTotal());
        assertEquals(pageSize, pageData.getRecords().size());
    }

}