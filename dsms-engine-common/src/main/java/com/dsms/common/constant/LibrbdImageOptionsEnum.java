/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.constant;

public enum LibrbdImageOptionsEnum {
    RBD_IMAGE_OPTION_FORMAT(0),
    RBD_IMAGE_OPTION_FEATURES(1),
    RBD_IMAGE_OPTION_ORDER(2),
    RBD_IMAGE_OPTION_STRIPE_UNIT(3),
    RBD_IMAGE_OPTION_STRIPE_COUNT(4),
    RBD_IMAGE_OPTION_JOURNAL_ORDER(5),
    RBD_IMAGE_OPTION_JOURNAL_SPLAY_WIDTH(6),
    RBD_IMAGE_OPTION_JOURNAL_POOL(7),
    RBD_IMAGE_OPTION_FEATURES_SET(8),
    RBD_IMAGE_OPTION_FEATURES_CLEAR(9),
    RBD_IMAGE_OPTION_DATA_POOL(10),
    RBD_IMAGE_OPTION_FLATTEN(11),
    RBD_IMAGE_OPTION_CLONE_FORMAT(12),
    RBD_IMAGE_OPTION_MIRROR_IMAGE_MODE(13);

    private final int value;

    LibrbdImageOptionsEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
